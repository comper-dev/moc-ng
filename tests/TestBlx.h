#pragma once
#include <BlxMocDefs.h>

class vec2
{
    BLX_CLASS
    
    double x, y;
    double getX() const { return x; }
    void setX(double a) { x = a; }
    double getY() const { return y; }
    void setY(double a) { y = a; }
    
    BLX_PROPERTY(double x READ getX WRITE setX)
    BLX_PROPERTY(double y READ getY WRITE setY)
};
