/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>

namespace StrUtils
{
    
    std::vector<std::string> split(const std::string &text, char sep, bool skipEmptyParts = true);
    
    inline bool
    endsWith(const std::string &str, const std::string &suffix)
    {
        return ( ( str.size() >= suffix.size() ) && (str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0) );
    }
    
    inline bool
    startsWith(const std::string& str, const std::string& prefix)
    {
        return str.substr(0,prefix.size()) == prefix;
    }
    
    
#ifdef _WIN32
    inline bool isAbsolute(const std::string& filePath)
    {
        return ((filePath.size() >= 3
                 && std::isalpha(filePath[0])
                 && filePath[1] == ':'
                 && filePath[2] == '/')
                || (filePath.size() >= 2
                    && filePath[0] == '/'
                    && filePath[1] == '/'));
    }
#else
    inline bool isAbsolute(const std::string& filePath)
    {
        return (!filePath.empty() && (filePath[0] == '/'));
    }
#endif
    inline bool isRelative(const std::string& filePath) {
        return !isAbsolute(filePath);
    }
    
    int compareCaseInsensitive(const std::string& s1, const std::string& s2);
    
    std::string cleanPath(const std::string &path, bool *ok = nullptr);
    
    std::string relativeFilePath(const std::string& dirPath, const std::string &absoluteFilePath);
} // StrUtils
