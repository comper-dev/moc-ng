/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _MSC_VER 
#pragma warning (push, 0)
#endif
#include <clang/Frontend/FrontendAction.h>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Tooling/Tooling.h>
#include <clang/Driver/Driver.h>
#include <clang/Driver/Compilation.h>
#include <clang/Driver/Tool.h>
#include <clang/Basic/DiagnosticIDs.h>
#include <clang/Lex/LexDiagnostic.h>
#include <clang/Sema/SemaDiagnostic.h>

#include <clang/Driver/Job.h>
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include <clang/Lex/Preprocessor.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/DeclCXX.h>
#include <llvm/Support/Host.h>
#include <llvm/Support/ThreadPool.h>
#ifdef _MSC_VER 
#pragma warning (pop)
#endif
#include <thread>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include "mocastconsumer.h"
#include "generator.h"
#include "mocppcallbacks.h"
#include "embedded_includes.h"
#include "stringutils.h"

using namespace StrUtils;

struct MocOptions {
    bool NoInclude = false;
    bool SilentWarnings = false;
    bool SilentErrors = false;
    std::vector<std::string> Includes;
    std::string Output;
    std::string OutputTemplateHeader;
    std::vector<std::pair<llvm::StringRef, llvm::StringRef>> MetaData;
    void addOutput(llvm::StringRef);
} ;


void MocOptions::addOutput(llvm::StringRef Out)
{
    if (Output.empty()) {
        Output = cleanPath(Out.str());
    } else if (OutputTemplateHeader.empty()) {
        OutputTemplateHeader = Out.str();
    } else {
        std::cerr << "moc-ng: Too many output file specified" << std::endl;
    }
}

/* Proxy that changes some errors into warnings  */
struct MocDiagConsumer : clang::DiagnosticConsumer {
    std::unique_ptr<DiagnosticConsumer> Proxy;
    MocOptions& Options;
    MocDiagConsumer(MocOptions* opts, std::unique_ptr<DiagnosticConsumer> Previous) : Proxy(std::move(Previous)), Options(*opts)  {}
    
    int HadRealError = 0;
    
#if CLANG_VERSION_MAJOR == 3 && CLANG_VERSION_MINOR <= 2
    DiagnosticConsumer* clone(clang::DiagnosticsEngine& Diags) const override {
        return new MocDiagConsumer { Proxy->clone(Diags) };
    }
#endif
    void BeginSourceFile(const clang::LangOptions& LangOpts, const clang::Preprocessor* PP = 0) override {
        Proxy->BeginSourceFile(LangOpts, PP);
    }
    void clear() override {
        Proxy->clear();
    }
    void EndSourceFile() override {
        Proxy->EndSourceFile();
    }
    void finish() override {
        Proxy->finish();
    }
    void HandleDiagnostic(clang::DiagnosticsEngine::Level DiagLevel, const clang::Diagnostic& Info) override {
        
        /* Moc ignores most of the errors since it even can operate on non self-contained headers.
         * So try to change errors into warning.
         */
        
        auto DiagId = Info.getID();
        auto Cat = Info.getDiags()->getDiagnosticIDs()->getCategoryNumberForDiag(DiagId);
        
        bool ShouldReset = false;
        
        if (DiagLevel >= clang::DiagnosticsEngine::Error ) {
            if (Cat == 2 || Cat == 4
                || DiagId == clang::diag::err_param_redefinition
                || DiagId == clang::diag::err_pp_expr_bad_token_binop
                || DiagId == clang::diag::err_use_of_tag_name_without_tag) {
                if (!HadRealError)
                    ShouldReset = true;
                DiagLevel = clang::DiagnosticsEngine::Warning;
            } else {
                HadRealError++;
            }
        }
        
        if ((DiagLevel == clang::DiagnosticsEngine::Warning && !Options.SilentWarnings) ||
            (DiagLevel == clang::DiagnosticsEngine::Error && !Options.SilentErrors)) {
            DiagnosticConsumer::HandleDiagnostic(DiagLevel, Info);
            Proxy->HandleDiagnostic(DiagLevel, Info);
        }
        
        if (ShouldReset) {
            // FIXME:  is there another way to ignore errors?
            const_cast<clang::DiagnosticsEngine *>(Info.getDiags())->Reset();
        }
    }
};



struct MocNGASTConsumer : public MocASTConsumer {
    std::string InFile;
    MocOptions& Options;
    MocNGASTConsumer(MocOptions* options, clang::CompilerInstance& ci, llvm::StringRef InFile) : MocASTConsumer(ci), InFile(InFile), Options(*options) { }
    
    
#if CLANG_VERSION_MAJOR == 3 && CLANG_VERSION_MINOR < 8
    // Clang 3.8 changed when Initialize is called. It is now called before the main file has been entered.
    // But with Clang < 3.8 it is called after, and PPCallbacks::FileChanged is not called when entering the main file
    void Initialize(clang::ASTContext& Ctx) override {
        MocASTConsumer::Initialize(Ctx);
        PPCallbacks->EnterMainFile(InFile);
    }
#endif
    
    bool shouldParseDecl(clang::Decl * D) override {
        // We only want to parse the Qt macro in classes that are in the main file.
        auto SL = D->getSourceRange().getBegin();
        SL = ci.getSourceManager().getExpansionLoc(SL);
        if (ci.getSourceManager().getFileID(SL) != ci.getSourceManager().getMainFileID())
            return false;
        return true;
    }
    
    void HandleTranslationUnit(clang::ASTContext& Ctx) override {
        
        if (ci.getDiagnostics().hasErrorOccurred())
            return;
        
        if (!objects.size() && !namespaces.size()) {
            std::cerr << "No relevant classes found. No output generated" << std::endl;
            /*ci.getDiagnostics().Report(ci.getSourceManager().getLocForStartOfFile(ci.getSourceManager().getMainFileID()),
             ci.getDiagnostics().getCustomDiagID(clang::DiagnosticsEngine::Warning,
             "No relevant classes found. No output generated"));*/
            //actually still create an empty file like moc does.
            ci.createOutputFile(Options.Output, false, true, "", "", false, false);
            return;
        }
        
        // createOutputFile returns a raw_pwrite_stream* before Clang 3.9, and a std::unique_ptr<raw_pwrite_stream> after
        auto OS = ci.createOutputFile(Options.Output, false, true, "", "", false, false);
        
        if (!OS) return;
        llvm::raw_ostream &Out = *OS;
        
        auto WriteHeader = [&](llvm::raw_ostream & Out) {
            Out <<  "/****************************************************************************\n"
            "** Meta object code from reading C++ file '" << InFile << "'\n"
            "**\n"
            "** Created by la-moc version " MOCNG_VERSION_STR " by LEFT-ANGLE. This is a fork of moc-ng by Woboq [https://woboq.com]\n"
            "** WARNING! All changes made in this file will be lost!\n"
            "*****************************************************************************/\n\n";
        };
        WriteHeader(Out);
        
        // Get a relative file path from the directory containing the output file to the original input file
        std::string relInFile;
        {
            std::string outputDir;
            std::size_t foundSlash = Options.Output.find_last_of("/");
            if (foundSlash != std::string::npos) {
                outputDir = Options.Output.substr(0, foundSlash);
            }
            relInFile = relativeFilePath(outputDir, InFile);
        }
        
        if (!Options.NoInclude) {
            for (auto &s : Options.Includes) {
                Out << s << "\n";
            }
            
            
            
            auto spos = relInFile.rfind('/');
            auto ppos = relInFile.rfind('.');
            // If it finished by .h, or if there is no dot after a slash,  we should include the source file
            if (ppos == std::string::npos || (spos != std::string::npos && spos > ppos) || endsWith(relInFile, ".h"))
                Out << "#include \"" << relInFile << "\"\n";
            if (Moc.HasPlugin)
                Out << "#include <QtCore/qplugin.h>\n";
            if (Moc.hasLAProperties)
                Out << "#include <PropertySystem/Property.h>\n";
        }
        
        Out << "#include <QtCore/qbytearray.h>\n";
        Out << "#include <iostream>\n";
        
        Out << "#if !defined(Q_MOC_OUTPUT_REVISION)\n"
        "#error \"The header file '" << relInFile << "' doesn't include <qobjectdefs.h>.\"\n"
        "#elif Q_MOC_OUTPUT_REVISION != " << mocOutputRevision << "\n"
        "#error \"This file was generated using la-moc " MOCNG_VERSION_STR ".\"\n"
        "#error \"It cannot be used with the include files from this version of Qt.\"\n"
        "#endif\n\n"
        "QT_BEGIN_MOC_NAMESPACE\n"
        "#ifdef QT_WARNING_DISABLE_DEPRECATED\n"
        "QT_WARNING_PUSH QT_WARNING_DISABLE_DEPRECATED\n"
        "#endif\n";
        
        decltype(OS) OS_TemplateHeader = nullptr;
        if (!Options.OutputTemplateHeader.empty()) {
            OS_TemplateHeader =
            ci.createOutputFile(Options.OutputTemplateHeader, false, true, "", "", false, false);
            if (!OS_TemplateHeader)
                return;
            WriteHeader(*OS_TemplateHeader);
            (*OS_TemplateHeader) << "QT_BEGIN_MOC_NAMESPACE\n"
            "#ifdef QT_WARNING_DISABLE_DEPRECATED\n"
            "QT_WARNING_PUSH QT_WARNING_DISABLE_DEPRECATED\n"
            "#endif\n";
        }
        
        
        // In order to use the ctor syntax when building
        // enums
        
        Out << "\n" << "using namespace LA_NS;\n";
        
        
        for (const ClassDef &Def : objects ) {
            Generator G(&Def, Out, Ctx, &Moc,
                        Def.Record->getDescribedClassTemplate() ? &*OS_TemplateHeader : nullptr);
            G.MetaData = Options.MetaData;
            if (llvm::StringRef(InFile).endswith("global/qnamespace.h"))
                G.IsQtNamespace = true;
            G.GenerateCode();
        };
        
        
        
        for (const NamespaceDef &Def : namespaces) {
            Generator G(&Def, Out, Ctx, &Moc);
            G.MetaData = Options.MetaData;
            G.GenerateCode();
        };
        
        llvm::StringRef footer =
        "QT_END_MOC_NAMESPACE\n"
        "#ifdef QT_WARNING_DISABLE_DEPRECATED\n"
        "QT_WARNING_POP\n"
        "#endif\n";
        Out << footer;
        if (OS_TemplateHeader) {
            (*OS_TemplateHeader) << footer;
        }
    }
};

class MocAction : public clang::ASTFrontendAction {
protected:
#if CLANG_VERSION_MAJOR == 3 && CLANG_VERSION_MINOR <= 5
    clang::ASTConsumer *
#else
    std::unique_ptr<clang::ASTConsumer>
#endif
    CreateASTConsumer(clang::CompilerInstance &CI, llvm::StringRef InFile) override {
        
        CI.getFrontendOpts().SkipFunctionBodies = true;
        CI.getPreprocessor().enableIncrementalProcessing(true);
        CI.getPreprocessor().SetSuppressIncludeNotFoundError(true);
        CI.getLangOpts().DelayedTemplateParsing = true;
        
        //enable all the extension
        CI.getLangOpts().MicrosoftExt = true;
        CI.getLangOpts().DollarIdents = true;
        CI.getLangOpts().CPlusPlus11 = true;
#if CLANG_VERSION_MAJOR == 3 && CLANG_VERSION_MINOR <= 5
        CI.getLangOpts().CPlusPlus1y = true;
#else
        CI.getLangOpts().CPlusPlus14 = true;
#endif
        CI.getLangOpts().GNUMode = true;
        
        CI.getDiagnostics().setClient(new MocDiagConsumer(Options, std::unique_ptr<clang::DiagnosticConsumer>(CI.getDiagnostics().takeClient())));
        
        return maybe_unique(new MocNGASTConsumer(Options, CI, InFile));
    }
    
public:
    
    MocAction(MocOptions* opt)
    : Options(opt)
    {
        
    }
    
    // CHECK
    virtual bool hasCodeCompletionSupport() const { return true; }
    
    MocOptions* Options = nullptr;
};

static void showVersion(bool /*Long*/) {
    std::cerr << "la-moc version " MOCNG_VERSION_STR " by LEFT-ANGLE. This is a fork of moc-ng by Woboq [https://woboq.com]" << std::endl;
}

static void showHelp() {
    std::cerr << "Usage moc: [options] <header-file>\n"
    "  -o<file>           write output to file rather than stdout\n"
    "  -I<dir>            add dir to the include path for header files\n"
    "  -E                 preprocess only; do not generate meta object code\n"
    "  -D<macro>[=<def>]  define macro, with optional definition\n"
    "  -U<macro>          undefine macro\n"
    "  -M<key=valye>      add key/value pair to plugin meta data\n"
    "  -i                 do not generate an #include statement\n"
    //               "  -p<path>           path prefix for included file\n"
    //               "  -f[<file>]         force #include, optional file name\n"
    //               "  -nn                do not display notes\n"
    //               "  -nw                do not display warnings\n"
    //               "  @<file>            read additional options from file\n"
    "  -v                 display version of moc-ng\n"
    "  -include <file>    Adds an implicit #include into the predefines buffer which is read before the source file is preprocessed\n"
    "  -s                 Does not print any warnings produced by clang diagnostics."
    /* undocumented options
     "  -W<warnings>       Enable the specified warning\n"
     "  -f<option>         clang option\n"
     "  -X<ext> <arg>      extensions arguments\n"
     */
    
    
    
    << std::endl;
    
    
    showVersion(false);
}

static int moc_main(std::vector<std::string> actualInput)
{
    bool NextArgNotInput = false;
    bool HasInput = false;
    std::string InputFile;
    bool PreprocessorOnly = false;
    
    std::vector<std::string> Argv;
    Argv.push_back(actualInput[0]);
    Argv.push_back("-x");  // Type need to go first
    Argv.push_back("c++");
#ifndef _MSC_VER
    Argv.push_back("-fPIE");
    Argv.push_back("-fPIC");
#else
    Argv.push_back("-pthread");
#endif
    Argv.push_back("-Wno-microsoft"); // get rid of a warning in qtextdocument.h
    Argv.push_back("-Wno-pragma-once-outside-header");
#if CLANG_VERSION_MAJOR == 3 && CLANG_VERSION_MINOR <= 5
    Argv.push_back("-std=c++11");
#else
    Argv.push_back("-std=c++1z");
#endif
    Argv.push_back("-stdlib=libc++");
    Argv.push_back("-DLA_MOC_RUN");
    //Argv.push_back("-DQ_MOC_RUN");
    
    MocOptions Options;
    
    for (std::size_t I = 1 ; I < actualInput.size(); ++I) {
        if (actualInput[I][0] == '-') {
            if (actualInput[I] == "--silent_warnings") {
                Options.SilentWarnings = true;
                continue;
            }
            if (actualInput[I] == "--silent_errors") {
                Options.SilentErrors = true;
                continue;
            }
            NextArgNotInput = false;
            switch (actualInput[I][1]) {
                case 'h':
                case '?':
                    showHelp();
                    return EXIT_SUCCESS;
                case 'v':
                    showVersion(true);
                    return EXIT_SUCCESS;
                case 'o':
                    if (actualInput[I][2]) {
                        Options.addOutput(&actualInput[I][2]);
                    }
                    else if (I + 1 < actualInput.size()) {
                        Options.addOutput(actualInput[I + 1]);
                        ++I;
                    }
                    continue;
                case 'i':
                    if (actualInput[I] == llvm::StringRef("-i")) {
                        Options.NoInclude = true;
                        continue;
                    } else if (actualInput[I] == llvm::StringRef("-include")) {
                        NextArgNotInput = true;
                        break;
                    }
                    goto invalidArg;
                case 'M': {
                    llvm::StringRef Arg;
                    if (actualInput[I][2]) Arg = &actualInput[I][2];
                    else if ((++I) < actualInput.size()) Arg = actualInput[I];
                    size_t Eq = Arg.find('=');
                    if (Eq == llvm::StringRef::npos) {
                        std::cerr << "moc-ng: missing key or value for option '-M'" << std::endl;
                        return EXIT_FAILURE;
                    }
                    Options.MetaData.push_back({Arg.substr(0, Eq), Arg.substr(Eq+1)});
                    continue;
                }
                case 'E':
                    PreprocessorOnly = true;
                    break;
                case 'F':
                    NextArgNotInput = (actualInput[I][2] == '\0');
                    
                    /*if (actualInput[I][2] != '\0') {
                     ++I;
                     }
                     continue;*/
                    break;
                case 'D':
                    break;
                case 'I':
                case 'U':
                    NextArgNotInput = (actualInput[I][2] == '\0');
                    if (!NextArgNotInput) {
                        std::string tmp = actualInput[I].substr(2);
                        tmp = cleanPath(tmp);
                        actualInput[I] = std::string("-I") + tmp;
                    } else {
                        actualInput[I + 1] = cleanPath(actualInput[I + 1]);
                    }
                    // Frameworks are sometimes passed with the I include ppath
                    if (endsWith(actualInput[I], ".framework")) {
                        std::size_t foundSlash = actualInput[I].find_last_of("/");
                        std::string frameworkname;
                        if (foundSlash != std::string::npos) {
                            std::size_t foundFW = actualInput[I].find(".framework");
                            if (foundFW != std::string::npos) {
                                frameworkname = actualInput[I].substr(foundSlash + 1, foundFW -1 - foundSlash);
                            }
                        }
                        Argv.push_back("-framework");
                        Argv.push_back(frameworkname);
                        
                    }
                    break;
                case 'X':
                    NextArgNotInput = true;
                    break;
                case 'f': //this is understood as compiler option rather than moc -f
                case 'W': // same
                    break;
                case 'n': //not implemented, silently ignored
                    continue;
                case '-':
                    if (llvm::StringRef(actualInput[I]) == "--include" ||
                        llvm::StringRef(actualInput[I]).startswith("--include=")) {
                        llvm::StringRef File;
                        if (llvm::StringRef(actualInput[I]).startswith("--include=")) {
                            File = llvm::StringRef(actualInput[I]).substr(llvm::StringRef("--include=").size());
                        } else if (I + 1 < actualInput.size()) {
                            File = llvm::StringRef(actualInput[++I]);
                        }
                        if (File.endswith("/moc_predefs.h")) {
                            // qmake generates moc_predefs with compiler defined stuff.
                            // We can't support it because everything is already pre-defined.
                            // So skip it.
                            continue;
                        }
                        Argv.push_back("-include");
                        Argv.push_back(File);
                        continue;
                    }
                    if (llvm::StringRef(actualInput[I]).startswith("--compiler-flavor")) {
                        if (llvm::StringRef(actualInput[I]) == "--compiler-flavor")
                            ++I;
                        // MSVC flavor not yet implemented
                        continue;
                    }
                    LLVM_FALLTHROUGH;
                default:
                invalidArg:
                    std::cerr << "moc-ng: Invalid argument '" << actualInput[I] << "'" << std::endl;
                    showHelp();
                    return EXIT_FAILURE;
            }
        } else if (!NextArgNotInput) {
            if (HasInput) {
                std::cerr << "error: Too many input files specified: " << actualInput[I - 1] <<  actualInput[I] << std::endl;
                return EXIT_FAILURE;
            }
            HasInput = true;
            InputFile = cleanPath(actualInput[I]);
        }
        Argv.push_back(actualInput[I]);
    } // foreach arg
    
    if (Options.Output.empty())
        Options.Output = "-";
    
    if (!HasInput)
        Argv.push_back("-");
    
    //FIXME
#if 0
    Argv.push_back("-I/usr/include/qt5");
    Argv.push_back("-I/usr/include/qt5/QtCore");
    Argv.push_back("-I/usr/include/qt");
    Argv.push_back("-I/usr/include/qt/QtCore");
#endif
    Argv.push_back("-I.uic"); // workaround the fact that the uic generated code cannot be found
    Argv.push_back("-I" LLVM_INCLUDE_DIR "/c++/v1");
    Argv.push_back("-I/builtins");
    Argv.push_back("-w");
#ifdef MACOS_SYSROOT
    Argv.push_back("-I" MACOS_SYSROOT "/usr/include");
#endif
    
    clang::FileManager FM({"."});
    FM.Retain();
    
    if (PreprocessorOnly) {
        Argv.push_back("-P");
        clang::tooling::ToolInvocation Inv(Argv,
                                           #if CLANG_VERSION_MAJOR >= 10
                                               std::make_unique<clang::PrintPreprocessedAction>(),
                                           #else
                                               new clang::PrintPreprocessedAction,
                                           #endif
                                          &FM);
        return !Inv.run();
    }
    
    Argv.push_back("-fsyntax-only");
    
    if (!endsWith(InputFile, "qobject.h") && !endsWith(InputFile, "qnamespace.h")) {
        // QObject should always be included
        // But not not for qobject.h (or we would not detect the main file correctly) or
        // qnamespace.h (that would break the Q_MOC_RUN workaround from MocPPCallbacks::EnterMainFile)
        Argv.push_back("-include");
        Argv.push_back("QtCore/qobject.h");
    }
    /*std::cout << "===CLANG ARGS===" << std::endl;
     for (auto it = Argv.begin(); it != Argv.end(); ++it) {
     std::cout << *it << std::endl;
     }
     std::cout << "===END CLANG ARGS===" << std::endl;*/
    clang::tooling::ToolInvocation Inv(Argv,
#if CLANG_VERSION_MAJOR >= 10
                                       std::make_unique<MocAction>(&Options),
#else
                                       new MocAction(&Options),
#endif
                                       &FM);
    
    for (std::size_t i = 0; i < EmbeddedFiles.size(); ++i) {
        if (!EmbeddedFiles[i].filename.empty()) {
            Inv.mapVirtualFile({ EmbeddedFiles[i].filename.c_str(),  EmbeddedFiles[i].filename.size() }, { EmbeddedFiles[i].unifiedContent.c_str() , EmbeddedFiles[i].unifiedContent.size() });
        }
    }
    
    return !Inv.run();
} // moc_main

int main(int argc, const char **argv)
{
    
    
    std::vector<std::string> singleInput;
    std::vector<std::vector<std::string>> multipleRuns;
    unsigned int numJobs = std::thread::hardware_concurrency();
    for (int i = 0; i < argc; ++i) {
        std::string a(argv[i]);
        if (i == 0) {
            continue;
        }
        if (argv[i][0] == '@') {
            if (!singleInput.empty()) {
                std::cerr << "cannot mix @ syntax with other arguments" << std::endl;
                return 1;
            }
            std::string paramsFile = a.substr(1);
            std::ifstream file;
            file.open(paramsFile, std::ifstream::in);
            if (!file.is_open()) {
                std::cerr << "Invalid parameters file: " << paramsFile << std::endl;
                return 1;
            }
            std::vector<std::string> thisRunArgs = {std::string(argv[0])};
            while (file.good()) {
                std::string line;
                std::getline(file, line);
                
                // split the argument, except if I U X D i
                if (line[0] == '-' &&
                    (line[1] == 'I' ||
                     line[1] == 'U' ||
                     line[1] == 'X' ||
                     line[1] == 'D' ||
                     line[1] == 'i')) {
                    thisRunArgs.push_back(line);
                } else {
                    std::vector<std::string> splits =  split(line, ' ');
                    thisRunArgs.reserve(thisRunArgs.size() + splits.size());
                    for (std::size_t c = 0; c < splits.size(); ++c) {
                        if (splits[c].empty()) {
                            continue;
                        }
                        thisRunArgs.push_back(splits[c]);
                    }
                }
            }
            
            multipleRuns.emplace_back(std::move(thisRunArgs));
        } else if (a == "-moc_jobs") {
            if (i + 1 >= argc) {
                std::cerr << "Expected num jobs" << std::endl;
                return 1;
            }
            numJobs = std::max(1u, std::min(std::thread::hardware_concurrency(), (unsigned int)std::atoi(argv[i+1])));
        } else {
            if (!multipleRuns.empty()) {
                std::cerr << "cannot mix @ syntax with other arguments" << std::endl;
                return 1;
            }
            singleInput.push_back(argv[i]);
        }
    }
    
    for (std::size_t i = 0; i < EmbeddedFiles.size(); ++i) {
        if (!EmbeddedFiles[i].filename.empty()) {
            for (std::size_t c = 0; c < EmbeddedFiles[i].content.size(); ++c) {
                EmbeddedFiles[i].unifiedContent += EmbeddedFiles[i].content[c];
            }
        }
    }
    
    if (multipleRuns.empty()) {
        if (moc_main(singleInput)) {
            return 1;
        }
    } else {
        singleInput.insert(singleInput.begin(), argv[0]);
       // llvm::ThreadPool pool(numJobs);
        std::vector<std::shared_future<int>> futures;
        futures.reserve(multipleRuns.size());
        for (auto& run : multipleRuns) {
            futures.emplace_back(std::async([&] () -> int {
                return moc_main(std::move(run));
            }));
        }
        for (auto& f : futures) {
            f.wait();
            if (f.get())
                return 1;
        }
    }
    
    return 0;
}
