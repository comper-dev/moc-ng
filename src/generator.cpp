/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "generator.h"
#include "mocng.h"
#include "qbjs.h"
#include <string>

#ifdef _MSC_VER 
#pragma warning (push, 0)
#endif
#include <clang/AST/DeclCXX.h>
#include <clang/AST/ASTContext.h>
#include <clang/AST/DeclTemplate.h>
#include <clang/Sema/Sema.h>
#ifdef _MSC_VER 
#pragma warning (pop)
#endif
#include <sstream>
#include <iostream>

// Remove the decltype if possible
static clang::QualType getDesugarType(const clang::QualType &QT) {
    if (auto DL = QT->getAs<clang::DecltypeType>()) {
        return DL->desugar();
    }
    return QT;
}

/* Wrapper for the change in the name in clang 3.5 */
template <typename T> static auto getResultType(T *decl) -> decltype(decl->getResultType())
{ return getDesugarType(decl->getResultType()); }
template <typename T> static auto getResultType(T *decl) -> decltype(decl->getReturnType())
{ return getDesugarType(decl->getReturnType()); }

/**
 * Return the type as writen in the file at the given SourceRange.
 * May return an empty string if the type was expended from macros.
 */
static std::string TypeStringFromSourceRange(clang::SourceRange Range, const clang::SourceManager &SM)
{
    if (Range.isInvalid() || !Range.getBegin().isFileID())
        return {};
    clang::FileID FID = SM.getFileID(Range.getBegin());
    if (FID != SM.getFileID(Range.getEnd()))
        return {};
    const llvm::MemoryBuffer *Buffer = SM.getBuffer(FID);
    const char *Buf = Buffer->getBufferStart();
    auto B = SM.getFileOffset(Range.getBegin());
    auto E = SM.getFileOffset(Range.getEnd());
    if (Buf[E] == '>') { // a type can be terminated be either a '>' or an identifier
        E++;
    } else {
        while (std::isalnum(Buf[E]) || Buf[E] == '\\' || Buf[E] == '_') E++;
    }
    return std::string(Buf+B, Buf+E);
}

// Returns true if the last argument of this mehod is a 'QPrivateSignal'
static bool HasPrivateSignal(const clang::CXXMethodDecl *MD) {
    if (MD && MD->getNumParams()) {
        clang::CXXRecordDecl* RD = MD->getParamDecl(MD->getNumParams()-1)->getType()->getAsCXXRecordDecl();
        return RD && RD->getIdentifier() && RD->getName() == "QPrivateSignal";
    }
    return false;
}

// Executes the 'Functor'  for each method,  including clones
template<typename T, typename F>
static void ForEachMethod(const std::vector<T> &V, F && Functor) {
    for(auto it : V) {
        int Clones = it->getNumParams() - it->getMinRequiredArguments();
        for (int C = 0; C <= Clones; ++C)
            Functor(it, C);
    }
}

// Count the number of method in the vector, including clones
template<typename T>
int CountMethod(const std::vector<T> &V) {
    int R  = 0;
    ForEachMethod(V, [&](const clang::CXXMethodDecl*, int) { R++; });
    return R;
}

// Count the total number of parametters in the vector
template<typename T> int AggregateParameterCount(const std::vector<T>& V) {
    int R = 0;
    ForEachMethod(V, [&](const clang::CXXMethodDecl *M, int Clone) {
        R += M->getNumParams() - Clone;
        R += 1; // return value;
        if (HasPrivateSignal(M))
            R--;
    });
    return R;
}

// Generate the data in the data array for the function in the given vector.
//  ParamIndex is a reference to the index in which to store the parametters.
template <typename T>
void Generator::GenerateFunctions(const std::vector<T> &V, const char* TypeName, QtMethodFlags Type, int& ParamIndex)
{
    if (V.empty())
        return;

    OS << "\n // " << TypeName << ": name, argc, parameters, tag, flags\n";

    ForEachMethod(V, [&](const clang::CXXMethodDecl *M, int Clone) {
        unsigned int Flags = (int)Type;
        if (M->getAccess() == clang::AS_private)
            Flags |= (int)QtMethodFlags::AccessPrivate;
        else if (M->getAccess() == clang::AS_public)
            Flags |= (int)QtMethodFlags::AccessPublic;
        else if (M->getAccess() == clang::AS_protected)
            Flags |= (int)QtMethodFlags::AccessProtected;

        if (Clone)
            Flags |= (int)QtMethodFlags::MethodCloned;

        for (auto attr_it = M->specific_attr_begin<clang::AnnotateAttr>();
             attr_it != M->specific_attr_end<clang::AnnotateAttr>();
             ++attr_it) {
            const clang::AnnotateAttr *A = *attr_it;
            if (A->getAnnotation() == "qt_scriptable") {
                Flags |= (int)QtMethodFlags::MethodScriptable;
            } else if (A->getAnnotation().startswith("qt_revision:")) {
                Flags |= (int)QtMethodFlags::MethodRevisioned;
            } else if (A->getAnnotation() == "qt_moc_compat") {
                Flags |= (int)QtMethodFlags::MethodCompatibility;
            }
        }

        int argc =  M->getNumParams() - Clone;
        if (HasPrivateSignal(M))
            argc--;

        std::string tag = Moc->GetTag(M->getSourceRange().getBegin(), Ctx.getSourceManager());
        OS << "    " << StrIdx(M->getNameAsString()) << ", " << argc << ", " << ParamIndex << ", " << StrIdx(tag) << ", 0x";
        OS.write_hex(Flags) << ",\n";
        ParamIndex += 1 + argc * 2;
    });
}

template <typename T>
void Generator::GenerateBlxFunctions(const std::vector<T> &V, const char *TypeName, BlxMetaMethodFlag Type, int &ParamIndex)
{
    if (V.empty())
        return;
    
    OS << "\n // " << TypeName << ": name, argc, parameters, tag, flags\n";
    
    ForEachMethod(V, [&](const clang::CXXMethodDecl *M, int Clone) {
        unsigned int Flags = (int)Type;
        if (M->getAccess() == clang::AS_private)
            Flags |= (int)BlxMetaMethodFlag::AccessPrivate;
        else if (M->getAccess() == clang::AS_public)
            Flags |= (int)BlxMetaMethodFlag::AccessPublic;
        else if (M->getAccess() == clang::AS_protected)
            Flags |= (int)BlxMetaMethodFlag::AccessProtected;
        
        if (Clone)
            Flags |= (int)QtMethodFlags::MethodCloned;
        
        for (auto attr_it = M->specific_attr_begin<clang::AnnotateAttr>();
             attr_it != M->specific_attr_end<clang::AnnotateAttr>();
             ++attr_it) {
            const clang::AnnotateAttr *A = *attr_it;
            if (A->getAnnotation().startswith("qt_revision:")) {
                Flags |= (int)BlxMetaMethodFlag::MethodRevisioned;
            }
        }
        
        int argc =  M->getNumParams() - Clone;
        if (HasPrivateSignal(M))
            argc--;
        
        std::string tag = Moc->GetTag(M->getSourceRange().getBegin(), Ctx.getSourceManager());
        OS << "    " << StrIdx(M->getNameAsString()) << ", " << argc << ", " << ParamIndex << ", " << StrIdx(tag) << ", 0x";
        OS.write_hex(Flags) << ",\n";
        ParamIndex += 1 + argc * 2;
    });
} // GenerateBlxFunctions

static bool IsIdentChar(char c) {
    return (c=='_' || c=='$' || (c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}

//Generate the type information for the argument
void Generator::GenerateTypeInfo(clang::QualType Type, CodeGenType genType)
{
    if (Type->isVoidType()) {
        if (genType == eCodeGenQt)
            OS << "QMetaType::Void";
        else
            OS << "std::uint32_t(BlxMetaTypeID::Void)";
        
        return;
    }

    // remove const or const &
    if (Type->isReferenceType() && Type.getNonReferenceType().isConstQualified())
        Type = Type.getNonReferenceType();
    Type.removeLocalConst();

    const clang::TypedefType * TT = Type->getAs<clang::TypedefType>();
    // Handle builtin types as QMetaType,  but ignores typedef their name is likely not registered
    //  (FIXME:  all the registered typedef such as unsigned int and qint64 should go there.
    if (Type->isBuiltinType() && (!TT)) {
        const clang::BuiltinType * BT = Type->getAs<clang::BuiltinType>();
        switch(+BT->getKind()) {
#define BUILTIN(Type, MetaTypeEnum) \
            case clang::BuiltinType::Type: \
                if (genType == eCodeGenQt) {\
                    OS << "QMetaType::"; \
                    OS << #Type; \
                } else { \
                    OS << "std::uint32_t(BlxMetaTypeID::"; \
                    OS << #MetaTypeEnum << ")"; \
                } \
                return;
            BUILTIN(Bool, Bool)
            BUILTIN(Int, Int64)
            BUILTIN(UInt, Uint64)
            BUILTIN(LongLong, Int64)
            BUILTIN(ULongLong, Uint64)
            BUILTIN(Double, Double)
            BUILTIN(Long, Int32)
            BUILTIN(Short, Int16)
            // Char?
            BUILTIN(ULong, Uint32)
            BUILTIN(UShort, Uint16)
            BUILTIN(UChar, Uint8)
            BUILTIN(Float, Float)
            BUILTIN(SChar, Int8)
#undef BUILTIN
        }
    }
    // TODO:  Find more QMetaType


    clang::PrintingPolicy Policy = PrintPolicy;
    Policy.SuppressScope = true;
    std::string TypeString = getDesugarType(Type).getAsString(Policy);

    // Remove the spaces;
    int k = 0;
    for (unsigned int i = 0; i < TypeString.size(); ++i) {
        char C = TypeString[i];
        if (C == ' ') {
            if (k == 0)
                continue;
            if (i+1 == TypeString.size())
                continue;
            char P = TypeString[k-1];
            char N = TypeString[i+1];
            if (!(IsIdentChar(P) && IsIdentChar(N))
                && !(P == '>' && N == '>'))
                continue;
        }
        TypeString[k++] = C;
    }
    TypeString.resize(k);

    //adjust unsigned
    unsigned int UPos = 0;
    while ((UPos = TypeString.find("unsigned ", UPos)) < TypeString.size()) {
        const int L = sizeof("unsigned ") - 1; // don't include \0
        llvm::StringRef R(&TypeString[UPos + L],
                          TypeString.size() - L);
        if (R.startswith("int") || (R.startswith("long") &&
            !R.startswith("long int") && !R.startswith("long long"))) {
            TypeString.replace(UPos, L, "u");
        }
        UPos++;
    }
    OS << "0x80000000 | " << StrIdx(TypeString);
}

// Generate the data in the data array for the parametters of functions in the vector
template <typename T>
void Generator::GenerateFunctionParameters(const std::vector< T* >& V, const char* TypeName, CodeGenType type)
{
    if (V.empty())
        return;

    OS << "\n // " << TypeName << ": parameters\n";

    ForEachMethod(V, [&](const clang::CXXMethodDecl *M, int Clone) {
        int argc =  M->getNumParams() - Clone;
        if (HasPrivateSignal(M))
            argc--;
        OS << "    ";
        //Types
        if (std::is_same<T, clang::CXXConstructorDecl>::value)
            OS << "0x80000000 | " << StrIdx("");
        else
            GenerateTypeInfo(getResultType(M), type);
        OS <<  ",";
        for (int j = 0; j < argc; j++) {
            OS << " ";
            GenerateTypeInfo(M->getParamDecl(j)->getOriginalType(), type);
            OS <<  ",";
        }

        //Names
        for (int j = 0; j < argc; j++) {
            auto P = M->getParamDecl(j);
            if (P->getIdentifier())
                OS << " " << StrIdx(P->getName()) << ",";
            else
                OS << " " << StrIdx("") << ",";
        }
        OS << "\n";
    });
}

// return true if a staticMetaObject is found in the bases
static bool hasStaticAnnotation(clang::QualType T, const char* keyIn) {
    auto RD = T->getAsCXXRecordDecl();
    if (!RD)
        return false;

    for (auto it = RD->decls_begin(); it != RD->decls_end(); ++it) {
        llvm::StringRef key;
        clang::Expr *subExpr;
        if (!MocNg::IsAnnotationStaticAssert(*it, &key, &subExpr))
            continue;
        
        if (llvm::StringRef(keyIn) == key) {
            return true;
        }
    }

    if (RD->getNumBases()) {
        return hasStaticAnnotation(RD->bases_begin()->getType(), keyIn);
    }
    return false;
}

// return true if a staticMetaObject is found in the bases
static bool hasStaticMetaObject(clang::QualType T) {
    auto RD = T->getAsCXXRecordDecl();
    if (!RD)
        return false;

    for (auto it = RD->decls_begin(); it != RD->decls_end(); ++it) {
        if (const clang::NamedDecl *Sub = llvm::dyn_cast<const clang::NamedDecl>(*it)) {
            if (Sub->getIdentifier() && Sub->getName() == "staticMetaObject")
                return true;
        }
    }

    if (RD->getNumBases()) {
        return hasStaticMetaObject(RD->bases_begin()->getType());
    }
    return false;
}

// return true if a staticMetaObject is found in the bases
static bool hasBlxStaticMetaObject(clang::QualType T) {
    auto RD = T->getAsCXXRecordDecl();
    if (!RD)
        return false;

    for (auto it = RD->decls_begin(); it != RD->decls_end(); ++it) {
        if (const clang::NamedDecl *Sub = llvm::dyn_cast<const clang::NamedDecl>(*it)) {
            if (Sub->getIdentifier() && (Sub->getName() == "blx_class"))
                return true;
        }
    }

    if (RD->getNumBases()) {
        return hasBlxStaticMetaObject(RD->bases_begin()->getType());
    }
    return false;
}

// return true if a staticMetaObject is found in the bases
static bool hasStaticTimeFunctions(clang::QualType T) {
    auto RD = T->getAsCXXRecordDecl();
    if (!RD)
        return false;

    for (auto it = RD->decls_begin(); it != RD->decls_end(); ++it) {
        if (const clang::NamedDecl *Sub = llvm::dyn_cast<const clang::NamedDecl>(*it)) {
            if (Sub->getIdentifier() && Sub->getName() == "sMetaClass")
                return true;
        }
    }

    if (RD->getNumBases()) {
        return hasStaticTimeFunctions(RD->bases_begin()->getType());
    }
    return false;
}

template <typename T>
static void PrintTemplateParamName(llvm::raw_ostream &Out, const T *D, bool PrintPack = false)
{
    if (auto II = D->getIdentifier()) {
        Out << II->getName();
    } else {
        Out << "T_" << D->getDepth() << "_" << D->getIndex();
    }
    if (PrintPack && D->isParameterPack()) {
        Out << "...";
    }
}

static void PrintTemplateParameters(llvm::raw_ostream &Out, clang::TemplateParameterList *List,
                                    clang::PrintingPolicy &PrintPolicy)
{
    Out << "template <";
    bool NeedComa = false;
    for (clang::NamedDecl *Param : *List) {
        if (NeedComa) Out << ", ";
        NeedComa = true;
        if (const auto *TTP = llvm::dyn_cast<clang::TemplateTypeParmDecl>(Param)) {
            if (TTP->wasDeclaredWithTypename()) {
                Out << "typename ";
            } else {
                Out << "class ";
            }
            if (TTP->isParameterPack())
                Out << "...";
            PrintTemplateParamName(Out, TTP);
        } else if (const auto *NTTP = llvm::dyn_cast<clang::NonTypeTemplateParmDecl>(Param)) {
            auto Type = NTTP->getType();
            bool Pack = NTTP->isParameterPack();
            if (auto *PET = Type->getAs<clang::PackExpansionType>()) {
                Pack = true;
                Type = PET->getPattern();
            }
            llvm::SmallString<25> Name;
            {
                llvm::raw_svector_ostream OS (Name);
                PrintTemplateParamName(OS, NTTP);
            }
            Type.print(Out, PrintPolicy, (Pack ? "... " : "") + Name);
        } else if (const auto *TTPD = llvm::dyn_cast<clang::TemplateTemplateParmDecl>(Param)) {
            PrintTemplateParameters(Out, TTPD->getTemplateParameters(), PrintPolicy);
            Out << "class ";
            if (TTPD->isParameterPack())
                Out << "... ";
            PrintTemplateParamName(Out, TTPD);
        }
    }
    Out << "> ";
}

Generator::Generator(const ClassDef* CDef, llvm::raw_ostream& OS, clang::ASTContext& Ctx,
                     MocNg* Moc, llvm::raw_ostream *OS_TemplateHeader)
    : Def(CDef), CDef(CDef), NDef(nullptr), OS(OS), OS_TemplateHeader(OS_TemplateHeader ? *OS_TemplateHeader : OS),
      Ctx(Ctx), PrintPolicy(Ctx.getPrintingPolicy()), Moc(Moc)
{
    PrintPolicy.SuppressTagKeyword = true;
    PrintPolicy.SuppressUnwrittenScope = true;
    PrintPolicy.AnonymousTagLocations = false;

    HasTemplateHeader = OS_TemplateHeader && (&OS != OS_TemplateHeader);
    
    {
        llvm::raw_string_ostream NameS(Name);
        CDef->Record->printName(NameS);
    }
    {
        llvm::raw_string_ostream QualNameS(QualName);
        CDef->Record->printQualifiedName(QualNameS, PrintPolicy);
    }
    
    if (CDef->Record->getNumBases()) {
//        auto Base = CDef->Record->bases_begin()->getTypeSourceInfo();

        for (auto it = CDef->Record->bases_begin(); it != CDef->Record->bases_end(); ++it) {
            // We need to try to get the type name as written. Because we don't want qualified name if
            // it was not qualified.  For example:
            //   namespace X { struct F; namespace Y { struct X; struct G : F { Q_OBJECT };  } }
            //   We don't want to use X::F  because X would be the struct and not the namespace
            auto baseName = TypeStringFromSourceRange(it->getTypeSourceInfo()->getTypeLoc().getSourceRange(), Ctx.getSourceManager());
            if (baseName.empty()) {
                baseName = it->getTypeSourceInfo()->getType().getAsString(PrintPolicy);
            }

            const bool baseHasStaticMetaObject = hasStaticMetaObject(it->getType());
            const bool baseHasStaticBlxMetaObject = hasBlxStaticMetaObject(it->getType());
            const bool baseHasStaticTimeFunctions =  hasStaticTimeFunctions(it->getType());
            const bool baseHasProperties = hasStaticAnnotation(it->getType(), "m_moc_properties");
            if (baseHasStaticMetaObject && qBaseName.empty()) {
                qBaseName = baseName;
            }
            if (baseHasProperties && propertyContainerBase.empty()) {
                propertyContainerBase = baseName;
            }

            if (baseHasStaticBlxMetaObject && blxBaseName.empty()) {
                blxBaseName = baseName;
            }
            
            if (baseHasStaticTimeFunctions && timeFunctionsBaseName.empty())
                timeFunctionsBaseName = baseName;
        }
    }

    QtMethodCount = CountMethod(CDef->QtSignals) + CountMethod(CDef->Slots) + CountMethod(CDef->QtMethods) + CDef->PrivateSlotCount;
    BlxMethodCount = CountMethod(CDef->BlxSignals) + CountMethod(CDef->BlxMethods);

    if (auto Tpl = CDef->Record->getDescribedClassTemplate()) {
        llvm::raw_string_ostream TemplatePrefixStream(TemplatePrefix);
        PrintTemplateParameters(TemplatePrefixStream, Tpl->getTemplateParameters(), PrintPolicy);
        llvm::raw_string_ostream TemplatePostfixStream(QualName);
        bool NeedComa = false;
        TemplatePostfixStream << "<";
        for (clang::NamedDecl *Param : *Tpl->getTemplateParameters()) {
            if (NeedComa) TemplatePostfixStream << ", ";
            NeedComa = true;
            if (const auto *TTP = llvm::dyn_cast<clang::TemplateTypeParmDecl>(Param)) {
                PrintTemplateParamName(TemplatePostfixStream, TTP, true);
            } else if (const auto *NTTP = llvm::dyn_cast<clang::NonTypeTemplateParmDecl>(Param)) {
                PrintTemplateParamName(TemplatePostfixStream, NTTP, true);
            } else if (const auto *TTPD = llvm::dyn_cast<clang::TemplateTemplateParmDecl>(Param)) {
                PrintTemplateParamName(TemplatePostfixStream, TTPD, true);
            }
        }
        TemplatePostfixStream << ">";
    }
}

Generator::Generator(const NamespaceDef* NDef, llvm::raw_ostream& OS, clang::ASTContext& Ctx, MocNg* Moc)
    : Def(NDef), CDef(nullptr), NDef(NDef), OS(OS), OS_TemplateHeader(OS), Ctx(Ctx),
      PrintPolicy(Ctx.getPrintingPolicy()), Moc(Moc)
{
    PrintPolicy.SuppressTagKeyword = true;
    PrintPolicy.SuppressUnwrittenScope = true;
    PrintPolicy.AnonymousTagLocations = false;
    HasTemplateHeader = false;

    {
        llvm::raw_string_ostream QualNameS(QualName);
        NDef->Namespace->printQualifiedName(QualNameS, PrintPolicy);
    }

    QtMethodCount = 0;
}



void Generator::GenerateCode()
{
    
     QualifiedClassNameIdentifier = QualName;
    if (CDef && CDef->Record->getDescribedClassTemplate()) {
        auto pos = QualifiedClassNameIdentifier.find('<');
        QualifiedClassNameIdentifier.resize(std::min(QualifiedClassNameIdentifier.size(), pos));
    }
    std::replace(QualifiedClassNameIdentifier.begin(), QualifiedClassNameIdentifier.end(), ':', '_');

    if ((NDef && NDef->hasQNamespace) || (CDef && (CDef->HasQGadget || CDef->HasQObject))) {
        GenerateCodeInternal(eCodeGenQt);
    }
    if ((NDef && NDef->hasBlxNamespace) || (CDef && (CDef->HasBlxClass || CDef->HasBlxValueType))) {
        GenerateCodeInternal(eCodeGenBlx);
    }
    if (CDef && CDef->hasTimeFunctionDefine)
        GenerateTimeFunctionData();

    GenerateLAProperties();

}

void Generator::GenerateCodeInternal(CodeGenType genType) {
    
    // Build the data array
    int Index = MetaObjectPrivateFieldCount;

    //Helper function which adds N to the index and return a value suitable to be placed in the array.
    auto getCurrentIndexAndIncr = [&](int numItems) {
        if (numItems == 0)
            return 0;
        int curIndex = Index;
        Index += numItems;
        return curIndex;
    };
    
    const bool qt = genType == eCodeGenQt;
    const char* genPrefix = qt ? "qt" : "blx";
    int MethodCount = qt ? QtMethodCount : BlxMethodCount;
    const std::vector<clang::CXXMethodDecl*>* Methods = nullptr;
    const std::vector<clang::CXXConstructorDecl*>* Constructors = nullptr;
    const std::vector<PropertyDef>* Properties = nullptr;
    const std::vector<clang::CXXMethodDecl*>* Signals = nullptr;
    int RevisionMethodCount = 0;
    int RevisionPropertyCount = 0;
    int NotifyCount = 0;
    if (CDef) {
        Methods = qt ? &CDef->QtMethods : &CDef->BlxMethods;
        Constructors = qt ? &CDef->QtConstructors : &CDef->BlxConstructors;
        Properties = qt ? &CDef->QtProperties : &CDef->BlxProperties;
        Signals = qt ? &CDef->QtSignals : &CDef->BlxSignals;
        RevisionMethodCount = qt ? CDef->QtRevisionMethodCount : CDef->BlxRevisionMethodCount;
        RevisionPropertyCount = qt ? CDef->QtRevisionPropertyCount : CDef->BlxRevisionPropertyCount;
        NotifyCount = qt ? CDef->QtNotifyCount : CDef->BlxNotifyCount;
    }
    
    llvm::StringRef Static;
    if (!HasTemplateHeader) {
        Static = "static ";
    } else {
        OS_TemplateHeader << "\nextern const std::uint32_t " << genPrefix << "_meta_data_" << QualifiedClassNameIdentifier << "[];\n";
    }

    const std::vector<std::pair<std::string, std::string>>* classInfo = nullptr;
    std::size_t classInfoSize = 0;
    if (CDef) {
        classInfo = (qt) ? &CDef->QtClassInfo : &CDef->BlxClassInfo;
        classInfoSize = classInfo->size();
    }
    int outputRev = qt ? QtOutputRevision : BlxOutputRevision;
    OS << "\n" << Static << "const std::uint32_t " << genPrefix << "_meta_data_" << QualifiedClassNameIdentifier << "[] = {\n"
          "    " << outputRev << ", // revision\n"
          "    " << StrIdx(QualName) << ", // classname\n"
          "    " << classInfoSize << ", " << getCurrentIndexAndIncr(classInfoSize * 2) << ", //classinfo\n";

    OS << "    " << MethodCount << ", " << getCurrentIndexAndIncr(MethodCount * 5) << ", // methods \n";

    if (CDef && RevisionMethodCount)
        Index += MethodCount;

    int ParamsIndex = Index;
    if (CDef) {
        int TotalParameterCount = AggregateParameterCount(*Signals) + AggregateParameterCount(CDef->Slots)
                                + AggregateParameterCount(*Methods) + AggregateParameterCount(*Constructors);
        for (const PrivateSlotDef &P : CDef->PrivateSlots)
            TotalParameterCount += 1 + P.Args.size() * (1 + P.NumDefault) - (P.NumDefault * (P.NumDefault + 1) / 2);
        Index += TotalParameterCount * 2 // type and parameter names
            - MethodCount - CountMethod(*Constructors);  // return parameter don't have names
    }

    if (CDef)
        OS << "    " << Properties->size() << ", " << getCurrentIndexAndIncr(Properties->size() * 3) << ", // properties \n";
    else
        OS << "    " << 0 << ", " << 0 << ", // properties \n";

    if (CDef && NotifyCount)
        Index += Properties->size();
    if (CDef && RevisionPropertyCount)
        Index += Properties->size();

    const std::vector<std::tuple<clang::EnumDecl*, std::string, bool>>& enums = (qt) ? Def->QtEnums : Def->BlxEnums;

    OS << "    " << enums.size() << ", " << getCurrentIndexAndIncr(enums.size() * 5) << ", // enums \n";
    int EnumIndex = Index;
    for (auto e : enums)
        for (auto it = std::get<0>(e)->enumerator_begin() ; it != std::get<0>(e)->enumerator_end(); ++it)
            Index += 2;

    int ConstructorCount = CDef ? CountMethod(*Constructors) : 0;
    OS << "    " << ConstructorCount << ", " << getCurrentIndexAndIncr(ConstructorCount * 5) << ", // constructors \n";

    int flags = 0;
    if (CDef && (CDef->HasQGadget/* || CDef->HasBlxValueType || CDef->HasBlxClass*/)) {
        // Ideally, all the classes could have that flag. But this broke classes generated
        // by qdbusxml2cpp which generate code that require that we call qt_metacall for properties
        flags |= (int)QtMetaObjectFlags::PropertyAccessInStaticMetaCall;
    }
    OS << "    " << flags << ", // flags \n";

    OS << "    " << (CDef ? CountMethod(*Signals) : 0) << ", // signalCount \n";

    if (classInfo && classInfo->size()) {
        OS << "\n  // classinfo: key, value\n";
        for (const auto &I : *classInfo)
            OS << "    " << StrIdx(I.first) << ", " << StrIdx(I.second) << ",\n";
    }

    if (CDef) {
        if (qt) {
            GenerateFunctions(*Signals, "signals", QtMethodFlags::MethodSignal, ParamsIndex);
            GenerateFunctions(CDef->Slots, "slots", QtMethodFlags::MethodSlot, ParamsIndex);
        }
        for (const PrivateSlotDef &P : CDef->PrivateSlots) {
            for (int Clone = 0; Clone <= P.NumDefault; ++Clone) {
                int argc = (P.Args.size() - Clone);
                OS << "    " << StrIdx(P.Name) << ", " << argc << ", " << ParamsIndex << ", 0, 0x";
                unsigned int Flag = (int)QtMethodFlags::AccessPrivate | (int)QtMethodFlags::MethodSlot;
                if (Clone) Flag |= (int)QtMethodFlags::MethodCloned;
                OS.write_hex(Flag) << ",\n";
                ParamsIndex += 1 + argc * 2;
            }
        }
        if (qt) {
            GenerateFunctions(*Methods, "methods", QtMethodFlags::MethodMethod, ParamsIndex);
        } else {
            GenerateBlxFunctions(*Methods, "methods", BlxMetaMethodFlag::MethodMethod, ParamsIndex);
        }


        if (RevisionMethodCount) {
            auto GenerateRevision = [&](const clang::CXXMethodDecl *M, int Clone) {
                llvm::StringRef SubStr = "0";
                for (auto attr_it = M->specific_attr_begin<clang::AnnotateAttr>();
                        attr_it != M->specific_attr_end<clang::AnnotateAttr>();
                        ++attr_it) {
                    const clang::AnnotateAttr *A = *attr_it;
                    if (A->getAnnotation().startswith("qt_revision:")) {
                        SubStr = A->getAnnotation().substr(sizeof("qt_revision:")-1);
                    }
                }
                OS << " " << SubStr << ",";
            };
            OS << "\n // method revisions\n    ";
            ForEachMethod(*Signals, GenerateRevision);
            OS << "\n    ";
            ForEachMethod(CDef->Slots, GenerateRevision);
            //OS << "\n    ";
            for (const PrivateSlotDef &P : CDef->PrivateSlots) {
                for (int Clone = 0; Clone <= P.NumDefault; ++Clone)
                    OS << " 0,    ";
            }
            OS << "\n    ";
            ForEachMethod(*Methods, GenerateRevision);
            OS << "\n";
        }

        GenerateFunctionParameters(*Signals, "signals", genType);
        GenerateFunctionParameters(CDef->Slots, "slots", genType);
        for (const PrivateSlotDef &P : CDef->PrivateSlots) {
            for (int Clone = 0; Clone <= P.NumDefault; ++Clone) {
                int argc = (P.Args.size() - Clone);
                OS << "    ";
                if (P.ReturnType == "void") OS << "QMetaType::Void";
                else OS << "0x80000000 | " << StrIdx(P.ReturnType);
                for (int j = 0; j < argc; j++) {
                    OS << ", ";
                    if (P.Args[j] == "void") OS << "QMetaType::Void";
                    else OS << "0x80000000 | " << StrIdx(P.Args[j]);
                }
                //Names
                for (int j = 0; j < argc; j++) {
                        OS << ", " << StrIdx("");
                }
                OS << ",\n";
            }
        }
        GenerateFunctionParameters(*Methods, "methods", genType);
        GenerateFunctionParameters(*Constructors, "constructors", genType);

        if (qt)
            GenerateProperties();
        else
            GenerateBlxProperties();
    }

    GenerateEnums(EnumIndex, genType);

    if (CDef) {
        if (qt) {
            GenerateFunctions(*Constructors, "constructors", QtMethodFlags::MethodConstructor, ParamsIndex);
        } else {
            GenerateBlxFunctions(*Constructors, "constructors", BlxMetaMethodFlag::MethodConstructor, ParamsIndex);
        }
    }

    OS << "\n    0    // eod\n};\n";

    // StringArray;

    int TotalLen = 1;
    for (const auto &S : Strings)
        TotalLen += S.size() + 1;

    OS_TemplateHeader << "struct " << genPrefix << "_meta_stringdata_" << QualifiedClassNameIdentifier << "_t {\n";
    if (genType == eCodeGenQt) {
        OS_TemplateHeader << "   QByteArrayData data[" << Strings.size() << "];\n";
    } else {
        OS_TemplateHeader << "   const std::uint32_t offsetsAndSize[" << Strings.size() * 2 << "];\n";
    }
    OS_TemplateHeader << "    char stringdata[" << TotalLen << "];\n"
          "};\n";
    if (HasTemplateHeader) {
        OS_TemplateHeader << "extern const " << genPrefix << "_meta_stringdata_" << QualifiedClassNameIdentifier
            << "_t " << genPrefix << "_meta_stringdata_"<<  QualifiedClassNameIdentifier << ";\n";
    }
    if (genType == eCodeGenQt) {
        OS << "#define QT_MOC_LITERAL(idx, ofs, len) \\\n"
              "    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \\\n"
              "    qptrdiff(offsetof(" << genPrefix << "_meta_stringdata_"<<  QualifiedClassNameIdentifier << "_t, stringdata) + ofs \\\n"
              "        - idx * sizeof(QByteArrayData)) \\\n"
        "    )\n";
    } else {
        OS << "#define QT_MOC_LITERAL(ofs, len) \\\n"
        "    std::uint32_t(offsetof(" << genPrefix << "_meta_stringdata_"<<  QualifiedClassNameIdentifier << "_t, stringdata) + ofs), len\n";
    }
    
    OS << Static << "const " << genPrefix << "_meta_stringdata_"<<  QualifiedClassNameIdentifier << "_t " << genPrefix << "_meta_stringdata_"<<  QualifiedClassNameIdentifier << " = {\n"
          "    {\n";
    int Idx = 0;
    int LitteralIndex = 0;
    for (const auto &S : Strings) {
        if (LitteralIndex)
            OS << ",\n";
        if (genType == eCodeGenQt) {
            OS << "QT_MOC_LITERAL("<< (LitteralIndex++) << ", " << Idx << ", " << S.size() << ")";
        } else {
            OS << "QT_MOC_LITERAL(" << Idx << ", " << S.size() << ")";
            ++LitteralIndex;
        }
        
        Idx += S.size() + 1;
    }
    OS << "\n    },\n    \"";
    int Col = 0;
    for (const auto &S : Strings) {
        if (Col && Col + S.size() >= 72) {
            OS << "\"\n    \"";
            Col = 0;
        } else if (S.size() && ((S[0] >= '0' && S[0] <= '9') || S[0] == '?')) {
            OS << "\"\"";
            Col += 2;
        }

        // Can't use write_escaped because of the trigraph
        for (unsigned i = 0, e = S.size(); i != e; ++i) {
            unsigned char c = S[i];
            switch (c) {
            case '\\': OS << '\\' << '\\'; break;
            case '\t': OS << '\\' << 't'; break;
            case '\n': OS << '\\' << 'n'; break;
            case '"': OS << '\\' << '"'; break;
            case '?':
                if (i != 0 && S[i-1] == '?') {
                    OS << '\\';
                    Col++;
                }
                OS << '?';
                break;
            default:
                if (std::isprint(c)) {
                    OS << c;
                    break;
                }
                // Use 3 character octal sequence
                OS << '\\' << char('0' + ((c >> 6) & 7)) << char('0' + ((c >> 3) & 7)) << char('0' + ((c >> 0) & 7));
                Col += 3;
            }
        }

        OS << "\\0";
        Col += 2 + S.size();
    }
    OS << "\"\n};\n"
          "#undef QT_MOC_LITERAL\n";

    const std::vector<clang::CXXRecordDecl *>& extraData = qt ? Def->QtExtra : Def->BlxExtra;
    if (!extraData.empty()) {
        if (HasTemplateHeader) {
            if (qt) {
                OS_TemplateHeader << "#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)\n";
                OS_TemplateHeader << "extern const QMetaObject * const " << genPrefix << "_meta_extradata_" << QualifiedClassNameIdentifier << "[];\n";
                OS_TemplateHeader << "#else\n";
                OS_TemplateHeader << "extern const QMetaObject::SuperData blx_meta_extradata_" << QualifiedClassNameIdentifier << "[];\n";
                OS_TemplateHeader << "#endif\n";
            } else {
                OS_TemplateHeader << "extern const BlxMetaObject::SuperData blx_meta_extradata_" << QualifiedClassNameIdentifier << "[];\n";
            }
        }

        if (qt) {
            OS_TemplateHeader << "#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)\n";
            OS_TemplateHeader << Static << " const QMetaObject * const qt_meta_extradata_" << QualifiedClassNameIdentifier << "[] = {\n";
            OS_TemplateHeader << "#else\n";
            OS_TemplateHeader << Static << " const QMetaObject::SuperData qt_meta_extradata_" << QualifiedClassNameIdentifier << "[] = {\n";
            OS_TemplateHeader << "#endif\n";
        } else {
            OS << Static << "const BlxMetaObject::SuperData blx_meta_extradata_" << QualifiedClassNameIdentifier << "[] = {\n" ;
        }
        for (clang::CXXRecordDecl *E : extraData) {
                //TODO: Check that extra is a QObject
            if (qt) {
                OS_TemplateHeader << "#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)\n";
                OS << "    &" << E->getQualifiedNameAsString() << "::staticMetaObject,\n";
                OS_TemplateHeader << "#else\n";
                OS << "    QMetaObject::SuperData::link<" << E->getQualifiedNameAsString() << "::staticMetaObject>(),\n";
                OS_TemplateHeader << "#endif\n";
            } else {
                OS << "    BlxMetaObject::SuperData::link<" << E->getQualifiedNameAsString();
                OS << "::staticBlxMetaObject>(),\n";
            }
        }

        OS << "    nullptr\n};\n";

    }

    if (IsQtNamespace) {
        OS_TemplateHeader << "\nconst QMetaObject QObject::staticQtMetaObject = {\n"
        "    { 0, qt_meta_stringdata_Qt.data, qt_meta_data_Qt, nullptr, nullptr, nullptr }\n};\n";
        return;
    }
    
    bool HasStaticMetaCall = ((qt && CDef && CDef->HasQObject) || (!qt && CDef && (CDef->HasBlxClass || CDef->HasBlxValueType)) || (Methods && !Methods->empty()) || (Properties && !Properties->empty()) || (Constructors && !Constructors->empty()));
    if (qt) {
        OS_TemplateHeader << "\n" << TemplatePrefix << "const QMetaObject " << QualName << "::staticMetaObject = {\n"
        "    {\n ";
        if (qBaseName.empty() || (CDef->HasQGadget && !BaseHasStaticMetaObject)) {
            OS_TemplateHeader << "nullptr";
        } else {
            OS_TemplateHeader << "#if QT_VERSION < QT_VERSION_CHECK(5, 14, 0)\n";
            OS_TemplateHeader << "&" << qBaseName << "::staticMetaObject\n";
            OS_TemplateHeader << "#else\n";
            OS_TemplateHeader << "QMetaObject::SuperData::link<" << qBaseName << "::staticMetaObject>()\n";
            OS_TemplateHeader << "#endif\n";
            
        }
        
        OS_TemplateHeader << ", qt_meta_stringdata_" << QualifiedClassNameIdentifier << ".data,\n"
        "      qt_meta_data_" << QualifiedClassNameIdentifier << ", ";
        
        if (HasStaticMetaCall)
            OS_TemplateHeader << "qt_static_metacall, ";
        else
            OS_TemplateHeader << "nullptr, ";
        
        if (!extraData.empty()) OS_TemplateHeader << "qt_meta_extradata_" << QualifiedClassNameIdentifier << ", ";
        else OS_TemplateHeader << "nullptr, ";
        OS_TemplateHeader << "nullptr}\n};\n";
    }
    else {
#if 0
        if (qt) {
            OS_TemplateHeader << TemplatePrefix << "const QMetaObject& " << QualName << "::getStaticMetaObject() {\n";
            OS_TemplateHeader << "    static const QMetaObject sMetaObject{\n"
            "    { ";
            if (BaseName.empty() || (CDef->HasQGadget && !BaseHasStaticMetaObject)) OS_TemplateHeader << "nullptr";
            else
                OS_TemplateHeader << "QMetaObject::SuperData::link<" << BaseName << "::getStaticMetaObject()>()";
            
            OS_TemplateHeader << ", qt_meta_stringdata_" << QualifiedClassNameIdentifier << ".data,\n"
            "      qt_meta_data_" << QualifiedClassNameIdentifier << ", ";
            
            if (HasStaticMetaCall) OS_TemplateHeader << "qt_static_metacall, ";
            else OS_TemplateHeader << "nullptr, ";
            
            if (!extraData.empty()) OS_TemplateHeader << "qt_meta_extradata_" << QualifiedClassNameIdentifier << ", ";
            else OS_TemplateHeader << "nullptr, ";
            OS_TemplateHeader << "nullptr}\n};    return sMetaObject;\n}\n";
            OS_TemplateHeader << "\n" << TemplatePrefix << "const QMetaObject& " << QualName << "::staticMetaObject = " << QualName << "::getStaticMetaObject();\n";
        } else
#endif
        {
            OS_TemplateHeader << "\n" << TemplatePrefix << "const BlxMetaObject " << QualName << "::staticBlxMetaObject = {\n"
            "    {\n ";
            
            /*OS_TemplateHeader << TemplatePrefix << "const BlxMetaObject* " << QualName << "::getStaticType() {\n";
            OS_TemplateHeader << "    static const BlxMetaObject sMetaObject\n"
            "    { ";
            */
            if (blxBaseName.empty() || (CDef->HasBlxValueType && !BaseHasStaticBlxMetaObject))
                OS_TemplateHeader << "nullptr";
            else
                OS_TemplateHeader << "BlxMetaObject::SuperData::link<" << blxBaseName << "::staticBlxMetaObject>()";
            
            OS_TemplateHeader << ", blx_meta_stringdata_" << QualifiedClassNameIdentifier << ".offsetsAndSize,\n"
            "      blx_meta_data_" << QualifiedClassNameIdentifier << ", ";
            
            if (HasStaticMetaCall)
                OS_TemplateHeader << "blx_static_metacall, ";
            else
                OS_TemplateHeader << "nullptr, ";
            
            if (!extraData.empty())
                OS_TemplateHeader << "blx_meta_extradata_" << QualifiedClassNameIdentifier;
            else
                OS_TemplateHeader << "nullptr";
            OS_TemplateHeader << "}\n};\n";
            //OS_TemplateHeader << "    return &sMetaObject;\n}\n";
            
            if (CDef) {
                OS_TemplateHeader << TemplatePrefix << "const BlxMetaObject* " << QualName << "::getStaticType() {\n";
                OS_TemplateHeader << "    return &staticBlxMetaObject;\n}\n";
            }
        }
        
    }

    if (CDef && ((qt && CDef->HasQObject) || (!qt && (CDef->HasBlxClass)))) {
        
        if (qt) {
            OS_TemplateHeader << TemplatePrefix << "const QMetaObject *" << QualName << "::metaObject() const\n{\n"
            "    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;\n}\n";
        } else {
            OS_TemplateHeader << TemplatePrefix << "const BlxMetaObject *" << QualName << "::getType() const\n{\n";
            OS_TemplateHeader << "    auto dynObject = getDynamicMetaObject();\n";
            OS_TemplateHeader << "    return dynObject ? dynObject : getStaticType();\n}\n";
        }
        
        
        OS_TemplateHeader <<  TemplatePrefix << "void *" << QualName << "::" << genPrefix << "_metacast(const char *_clname)\n{\n"
        "    if (!_clname) return 0;\n"
        "    if (!strcmp(_clname, " << genPrefix << "_meta_stringdata_" << QualifiedClassNameIdentifier << ".stringdata))\n"
        "        return static_cast<void*>(this);\n";
        
        if (CDef->Record->getNumBases() > 1) {
            for (auto BaseIt = CDef->Record->bases_begin()+1; BaseIt != CDef->Record->bases_end(); ++BaseIt) {
                if (BaseIt->getAccessSpecifier() == clang::AS_private)
                    continue;
                auto B = BaseIt->getType().getAsString(PrintPolicy);
                OS_TemplateHeader << "    if (!qstrcmp(_clname, \"" << B << "\"))\n"
                "        return static_cast< " << B << "*>(this);\n";
            }
        }
        
        for (const auto &Itrf : CDef->Interfaces) {
            OS_TemplateHeader << "    if (!qstrcmp(_clname, qobject_interface_iid< " << Itrf << " *>()))\n"
            "        return static_cast< " << Itrf << "  *>(this);\n";
        }

        const std::string baseName = qt ? qBaseName : blxBaseName;
        
        if (baseName.empty()) {
            OS_TemplateHeader << "    return 0;\n}\n";
        }
        else {
            OS_TemplateHeader << "    return "<< baseName <<"::" << genPrefix << "_metacast(_clname);\n"
            "}\n";
        }

        if (qt) {
            GenerateMetaCall();
            GenerateStaticMetaCall();
        } else {
            if (CDef->HasBlxClass)
                GenerateBlxMetaCall();
            
            GenerateBlxStaticMetaCall();
        }
        
        if (qt) {
            int SigIdx = 0;
            for (const clang::CXXMethodDecl *MD : *Signals) {
                GenerateSignal(MD, SigIdx);
                SigIdx += 1 + MD->getNumParams() - MD->getMinRequiredArguments();
            }
        }
    } else if (HasStaticMetaCall) {
        if (qt)
            GenerateStaticMetaCall();
        else
            GenerateBlxStaticMetaCall();
    }
    

    if (CDef && !CDef->Plugin.IID.empty()) {
        OS << "\nQT_PLUGIN_METADATA_SECTION const unsigned int qt_section_alignment_dummy = 42;\n"
              "#ifdef QT_NO_DEBUG\n";
        GeneratePluginMetaData(false);
        OS << "#else\n";
        GeneratePluginMetaData(true);
        OS << "#endif\n";
		auto qualNameToken = QualName;
		auto foundQual = qualNameToken.find("::");
		if (foundQual != std::string::npos)
			qualNameToken.replace(foundQual, 2, "_");
        OS << "QT_MOC_EXPORT_PLUGIN(" << QualName << ", " << qualNameToken  << ")\n\n";
    }
    
}

void Generator::GenerateLAProperties()
{

    if (!CDef || (CDef->LAAttachedPropertiesType.empty() && !CDef->has_la_properties)) {
        return;
    }

    // We have an attached type
    if (!CDef->LAAttachedPropertiesType.empty()) {
        OS_TemplateHeader << TemplatePrefix << CDef->LAAttachedPropertiesType << "* " << QualName <<
        "::qmlAttachedProperties(QObject *object) {\n";
        OS_TemplateHeader <<"    auto isRegistry = dynamic_cast<LA_NS::PropertyRegistryBase*>(object);\n";
        OS_TemplateHeader <<"    QString providerTypename = PropertyRegistryBase::getMetaObjectNameForQMLObject(&staticMetaObject);\n";
        OS_TemplateHeader <<"    " << CDef->LAAttachedPropertiesType << "* ret = nullptr;\n";
        OS_TemplateHeader <<"    if (isRegistry) {\n";
        OS_TemplateHeader <<"        auto ptr = make_shared<" << CDef->LAAttachedPropertiesType << ">();\n";
        OS_TemplateHeader <<"        isRegistry->registerAttachedProperties(ptr, providerTypename);\n";
        OS_TemplateHeader <<"        ret = ptr.get();\n";
        OS_TemplateHeader <<"    } else { \n";
        OS_TemplateHeader <<"        ret = " << CDef->LAAttachedPropertiesType << "::createAttached(object, providerTypename);\n";
        OS_TemplateHeader <<"    }\n";
        OS_TemplateHeader <<"    getQmlObjectOwnership(ret);\n";
        OS_TemplateHeader <<"    return ret;\n";
        OS_TemplateHeader <<"}\n";
        OS_TemplateHeader << TemplatePrefix << CDef->LAAttachedPropertiesType << "* " << QualName <<
        "::getAttached(LA_NS::PropertyRegistryBase* object) {\n";
        OS_TemplateHeader <<"    auto ret = dynamic_cast<" << CDef->LAAttachedPropertiesType << "*>(qmlAttachedPropertiesObject<" << QualName << ">(object, false)); \n";
        OS_TemplateHeader <<"    if (!ret || !ret->arePropertiesInstalled()) { \n";
        OS_TemplateHeader <<"        return nullptr; \n";
        OS_TemplateHeader <<"    } else { \n";
        OS_TemplateHeader <<"        return ret; \n";
        OS_TemplateHeader <<"    } \n";
        OS_TemplateHeader <<"}\n";
        OS_TemplateHeader << TemplatePrefix << CDef->LAAttachedPropertiesType << "* " << QualName <<
        "::getOrCreateAttached(LA_NS::PropertyRegistryBase* object) {\n";
        OS_TemplateHeader <<"    LA_NS::ensureQQmlContext(object); \n";
        OS_TemplateHeader <<"    auto ret = dynamic_cast<" << CDef->LAAttachedPropertiesType << "*>(qmlAttachedPropertiesObject<" << QualName << ">(object, true)); \n";
        /*we are passing create=true so it should always exist: the implementation of qmlAttachedProperties
         must always create an object, otherwise we would crash qml.*/
        OS_TemplateHeader <<"    assert(ret); \n";
        OS_TemplateHeader <<"    if (!ret) { \n";
        OS_TemplateHeader <<"        return nullptr; \n";
        OS_TemplateHeader <<"    } \n";
        OS_TemplateHeader <<"    if (!ret->arePropertiesInstalled()) {\n";
        OS_TemplateHeader <<"        ret->ensureProperties(); \n";
        OS_TemplateHeader <<"    }\n";
        OS_TemplateHeader <<"    return ret; \n";
        OS_TemplateHeader <<"} \n";
    }

    if (!CDef->has_la_properties) {
        return;
    }
    
    OS_TemplateHeader << TemplatePrefix << "const QStringList& " << QualName << "::getAllMocPropertyNames() {\n";
       OS_TemplateHeader << "    static QStringList s;\n";
       if (CDef->LAProperties.size() > 0) {
           OS_TemplateHeader << "    static std::once_flag f;\n";
           OS_TemplateHeader << "    std::call_once(f, [&] {\n";
           OS_TemplateHeader << "        s.reserve(" << CDef->LAProperties.size() << ");\n";
           for (auto& p : CDef->LAProperties) {
               if (p.protocolVersion == 1)
                   OS_TemplateHeader << "        s.push_back(\"" << p.camelCaseName << "\");\n";
               else
                   OS_TemplateHeader << "        s.push_back(\"" << p.lowercaseName << "\");\n";
           }
           OS_TemplateHeader << "    });\n";
       }
       OS_TemplateHeader << "    return s;\n";
       OS_TemplateHeader << "}\n";
       
    
    // Generate definition of ensureMocPropertiesInitialized().
    OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::ensureMocPropertiesInitialized()\n{\n";
    
    if (!propertyContainerBase.empty()) {
        OS_TemplateHeader << "      " << propertyContainerBase << "::ensureMocPropertiesInitialized();\n";
    }
    if (!blxBaseName.empty()) {
        OS_TemplateHeader << "      " << blxBaseName << "::ensureMocPropertiesInitialized();\n";
    }
    
    if (!timeFunctionsBaseName.empty()) {
        OS_TemplateHeader << "     " << timeFunctionsBaseName << "::ensureMocPropertiesInitialized();\n";
    }

    if (CDef->LAProperties.size() > 0) {
        for (std::size_t i = 0; i < CDef->LAProperties.size(); ++i) {
            
            auto& prop = CDef->LAProperties[i];
            const bool useLowerCase = prop.protocolVersion >= 2;
            OS_TemplateHeader << "    {\n";

            if (prop.isList) {
                OS_TemplateHeader << "         auto p = createPropertyList<" << prop.paramType << ">(kProp" << prop.pascalCaseName << "s);\n";
                OS_TemplateHeader << "        assert(p);\n";

            } else {
                OS_TemplateHeader << "         auto p = createProperty<" << prop.paramType;
                if (prop.readOnly) {
                    OS_TemplateHeader << ", PropertyBase::NoSetter & " << QualName << "::Property_Flags_v";
                }
                if (!useLowerCase) {
                    OS_TemplateHeader << ">(kProp" << prop.pascalCaseName << ");\n";
                } else {
                    OS_TemplateHeader << ">(kProp_" << prop.lowercaseName << ");\n";
                }
                OS_TemplateHeader << "        assert(p);\n";
                if (!prop.defaultValue.empty())
                    OS_TemplateHeader << "        p->setValue(" << prop.defaultValue << ");\n";
                
                if (CDef->HasQObject) {
                    if (!useLowerCase) {
                        OS_TemplateHeader << "        connect(p, &PropertyBase::valueChanged, this, &" << QualName << "::" << prop.camelCaseName << "ValueChanged);\n";
                    } else {
                        OS_TemplateHeader << "        connect(p, &PropertyBase::valueChanged, this, &" << QualName << "::" << prop.lowercaseName << "_value_changed);\n";
                    }
                }
               
            }
            OS_TemplateHeader << "        onPropertyCreated(p);\n";
                           OS_TemplateHeader << "    }\n";
        }
    }

    OS_TemplateHeader << "}\n\n";


    // Iterate over properties.
    for (std::size_t i = 0; i < CDef->LAProperties.size(); ++i) {
        
        const LAPropertyDef& p = CDef->LAProperties[i];
        const bool useLowerCase = p.protocolVersion >= 2;
        
        
        if (p.isList) {
            OS_TemplateHeader << TemplatePrefix << "const char* " << QualName << "::kProp" << p.pascalCaseName << "s = \"" << p.camelCaseName << "s\";\n\n";
            
            // PropertyList only support pascal case convention
            OS_TemplateHeader << TemplatePrefix << "PropertyList<" << p.paramType << ">* " << QualName << "::get" << p.pascalCaseName << "sProperty() const\n{\n";
            
            OS_TemplateHeader << "     return getPropertyList<" << p.paramType << ">(kProp" << p.pascalCaseName << "s);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::set" << p.pascalCaseName << "At(unsigned int index, const " << p.paramType << "& value)\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "          p->setValueAt(index, value);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::append" << p.pascalCaseName << "(const " << p.paramType << "& value)\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "          p->appendElement(value);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::insert" << p.pascalCaseName << "(const " << p.paramType << "& value, int index)\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "          p->insertElement(value, index);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::remove" << p.pascalCaseName << "(unsigned int index)\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "          p->removeElement(index);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::clear" << p.pascalCaseName << "s()\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "          p->clear();\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::move" << p.pascalCaseName << "(unsigned int oldIndex, unsigned int newIndex)\n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader  << "    if (p)\n";
            OS_TemplateHeader << "           p->moveElement(oldIndex, newIndex);\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << p.paramType  << " "<< QualName << "::get" << p.pascalCaseName << "At(unsigned int index) const \n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader << "     if (p)\n";
            OS_TemplateHeader << "         return p->getValueAt(index);\n";
            OS_TemplateHeader << "     else\n";
            OS_TemplateHeader << "         return " << p.paramType << "();\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "std::size_t " << QualName << "::getNum" << p.pascalCaseName << "() const \n{\n";
            OS_TemplateHeader << "     auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader << "     if (p)\n";
            OS_TemplateHeader << "         return p->getCount();\n";
            OS_TemplateHeader << "     else\n";
            OS_TemplateHeader << "         return 0;\n";
            OS_TemplateHeader << "}\n\n";
            
            OS_TemplateHeader << TemplatePrefix << "def::vector<" << p.paramType << "> " << QualName << "::get" << p.pascalCaseName << "s() const\n{\n";
            OS_TemplateHeader << "    auto p = get" << p.pascalCaseName << "sProperty();\n";
            OS_TemplateHeader << "    if (p)\n";
            OS_TemplateHeader << "         return p->getValues();\n";
            OS_TemplateHeader << "     else\n";
            OS_TemplateHeader << "         return def::vector<" << p.paramType << ">();\n";
            OS_TemplateHeader << "}\n\n";
            
        } else {
            // Initialize the static variable that contains the property name
            // string.
            if (!useLowerCase) {
                OS_TemplateHeader << TemplatePrefix << "const char* " << QualName << "::kProp" << p.pascalCaseName << " = \"" << p.camelCaseName << "\";\n\n";
            } else {
                OS_TemplateHeader << TemplatePrefix << "const char* " << QualName << "::kProp_" << p.lowercaseName << " = \"" << p.lowercaseName << "\";\n\n";
            }
            
            // Generate the param getter.
            {
                
                OS_TemplateHeader << TemplatePrefix << "Property<" << p.paramType << ", " << QualName << "::Property_Flags_v";
                if (p.readOnly)
                    OS_TemplateHeader << "& PropertyBase::NoSetter";
                
                if (!useLowerCase) {
                    OS_TemplateHeader << ">* " << QualName << "::get" << p.pascalCaseName << "Property" << "() const\n{\n";
                } else {
                    OS_TemplateHeader << ">* " << QualName << "::get_" << p.lowercaseName << "_property" << "() const\n{\n";
                }
                OS_TemplateHeader << "    auto p = getPropertyWithFlags<" << p.paramType;
                if (p.readOnly) {
                    OS_TemplateHeader << ", PropertyBase::NoSetter & " << QualName << "::Property_Flags_v";
                }
                if (!useLowerCase) {
                    OS_TemplateHeader << ">(kProp" << p.pascalCaseName << ");\n";
                } else {
                    OS_TemplateHeader << ">(kProp_" << p.lowercaseName << ");\n";
                }
                
                OS_TemplateHeader << "    if (!p) {\n";
                OS_TemplateHeader << "        auto thisUnConst = const_cast<" << QualName << "*>(this);\n";
                OS_TemplateHeader << "        p = thisUnConst->createProperty<" << p.paramType;
                if (p.readOnly) {
                    OS_TemplateHeader << ", PropertyBase::NoSetter & " << QualName << "::Property_Flags_v";
                }
                if (!useLowerCase) {
                    OS_TemplateHeader << ">(kProp" << p.pascalCaseName << ");\n";
                } else {
                    OS_TemplateHeader << ">(kProp_" << p.lowercaseName << ");\n";
                }
                OS_TemplateHeader << "        assert(p);\n";
                if (CDef->HasQObject) {
                    if (!useLowerCase) {
                        OS_TemplateHeader << "        connect(p, &PropertyBase::valueChanged, thisUnConst, &" << QualName << "::" << p.camelCaseName << "ValueChanged);\n";
                    } else {
                        OS_TemplateHeader << "        connect(p, &PropertyBase::valueChanged, thisUnConst, &" << QualName << "::" << p.lowercaseName << "_value_changed);\n";
                    }
                }
                OS_TemplateHeader << "        thisUnConst->onPropertyCreated(p);\n";
                OS_TemplateHeader << "    }\n";
                OS_TemplateHeader << "    return p;\n";
                OS_TemplateHeader << "}\n\n";
            }
            
            // Generate the property value getter.
            {
                if (!useLowerCase) {
                    OS_TemplateHeader << TemplatePrefix << p.paramType << " " << QualName << "::get" << p.pascalCaseName << "() const\n{\n";
                    OS_TemplateHeader << "    auto p = get" << p.pascalCaseName << "Property();\n";
                } else {
                    OS_TemplateHeader << TemplatePrefix << p.paramType << " " << QualName << "::get_" << p.lowercaseName << "() const\n{\n";
                    OS_TemplateHeader << "    auto p = get_" << p.lowercaseName << "_property();\n";
                    
                }
                OS_TemplateHeader << "    if (p) {\n";
                OS_TemplateHeader << "        return p->getValue();\n";
                OS_TemplateHeader << "    }\n";
                OS_TemplateHeader << "    return {};\n";
                OS_TemplateHeader << "}\n\n";
            }
            
            {
                if (!useLowerCase) {
                    OS_TemplateHeader << TemplatePrefix << p.paramType << "* " << QualName << "::get" << p.pascalCaseName << "Ref() const\n{\n";
                    OS_TemplateHeader << "    auto p = get" << p.pascalCaseName << "Property();\n";
                } else {
                    OS_TemplateHeader << TemplatePrefix << p.paramType << "* " << QualName << "::get_" << p.lowercaseName << "_ref() const\n{\n";
                    OS_TemplateHeader << "    auto p = get_" << p.lowercaseName << "_property();\n";
                }
                OS_TemplateHeader << "    if (p) {\n";
                OS_TemplateHeader << "        return p->getValuePointer();\n";
                OS_TemplateHeader << "    }\n";
                OS_TemplateHeader << "    return nullptr;\n";
                OS_TemplateHeader << "}\n\n";
            }
            
            // Generate the param value setter.
            if (!p.readOnly) {
                if (!useLowerCase) {
                    OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::set" << p.pascalCaseName << "(const " << p.paramType << "& value)\n{\n";
                    OS_TemplateHeader << "    auto p = get" << p.pascalCaseName << "Property();\n";
                } else {
                    OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::set_" << p.lowercaseName << "(const " << p.paramType << "& value)\n{\n";
                    OS_TemplateHeader << "    auto p = get_" << p.lowercaseName << "_property();\n";
                }
                
                OS_TemplateHeader << "    if (p) {\n";
                OS_TemplateHeader << "        p->setValue(value);\n";
                OS_TemplateHeader << "    }\n";
                OS_TemplateHeader << "}\n\n";
            }
        } // isList

    } // foreach property
}

void
Generator::GenerateTimeFunctionData()
{
    if (!CDef || !CDef->hasTimeFunctionDefine) {
        return;
    }

    
    OS_TemplateHeader << "\n" << TemplatePrefix << "const TimeFunctionMetaClass " << QualName << "::sMetaClass = TimeFunctionMetaClass(\"" << QualName<< "\");\n";
    // Generate definition of getUniqueClassName().
    OS_TemplateHeader << TemplatePrefix << "const TimeFunctionMetaClass& " << QualName << "::getMetaClass() const\n{\n";
    OS_TemplateHeader << "     return sMetaClass;\n";
    OS_TemplateHeader << "}\n\n";

    
    OS_TemplateHeader << TemplatePrefix << "const QStringList& " << QualName << "::getAllMocParameterNames() {\n";
    OS_TemplateHeader << "    static QStringList s;\n";
    if (CDef->TimeFunctionParams.size() > 0) {
        OS_TemplateHeader << "    static std::once_flag f;\n";
        OS_TemplateHeader << "    std::call_once(f, [&] {\n";
        OS_TemplateHeader << "        s.reserve(" << CDef->TimeFunctionParams.size() << ");\n";
        for (auto& p : CDef->TimeFunctionParams) {
            for (int i = 0; i < p.arrayDimension; ++i) {
                std::stringstream ss;
                ss << p.lowerCaseName;
                if (p.arrayDimension > 1) {
                    // Append the bare property name that represents the multi-dimensional function.
                    OS_TemplateHeader << "        s.push_back(\"" << p.lowerCaseName << "\");\n";
                    ss << "_dim_" << i;
                }
                std::string name = ss.str();
                OS_TemplateHeader << "        s.push_back(\"" << name << "\");\n";
            }
     
        }
        OS_TemplateHeader << "    });\n";
    }
    OS_TemplateHeader << "    return s;\n";
    OS_TemplateHeader << "}\n";
    
    
    // Generate definition of ensureMocPropertiesInitialized().
    OS_TemplateHeader << TemplatePrefix << "void " << QualName << "::ensureMocParametersInitialized()\n{\n";

    if (!timeFunctionsBaseName.empty()) {
        OS_TemplateHeader << "    " << timeFunctionsBaseName << "::ensureMocParametersInitialized();\n";
    }
    
    if (CDef->TimeFunctionParams.size() > 0) {
        for (std::size_t i = 0; i < CDef->TimeFunctionParams.size(); ++i) {
            auto& param = CDef->TimeFunctionParams[i];
            if (param.isImage) {
                OS_TemplateHeader << "    {\n";

                OS_TemplateHeader <<   "     auto p = dyn_cast<ImageEffectParameter>(addParameter(make_unique<ImageEffectParameter>(), kParam_" << param.lowerCaseName << ", getMetaTypeID<ImageEffectValue>()));\n";
                bool hasFlag = false;
                if (param.isMask) {
                    OS_TemplateHeader << "         p->setFlagEnabled(ImageEffectParameter::Flag::IsMask, true);\n";
                    hasFlag = true;
                }
                if (param.isMandatory) {
                    OS_TemplateHeader << "         p->setFlagEnabled(TimeFunctionParameter::Flag::Mandatory, true);\n";
                    hasFlag = true;
                }
                
                if (param.supportedDepths) {
                    hasFlag = true;
                    OS_TemplateHeader << "         p->setSupportedBitDepths(";
                    int nVals = 0;
                    static const char* const values[4] = {"BitDepth::Byte", "BitDepth::UShort", "BitDepth::Half", "BitDepth::Float"};
                    for (int i = 0; i < 4; ++i) {
                        if ((*param.supportedDepths)[i]) {
                            if (nVals > 0)
                                OS_TemplateHeader << "| ";
                            OS_TemplateHeader << values[i];
                            ++nVals;
                        }
                    }
                    OS_TemplateHeader << ");\n";
                }
                if (param.supportedChans) {
                    hasFlag = true;
                    OS_TemplateHeader << "         p->setSupportedNumChannels(";
                    int nVals = 0;
                    static const char* const values[4] = {"ChannelFormat::Alpha", "ChannelFormat::RG", "ChannelFormat::RGB", "ChannelFormat::RGBA"};
                    for (int i = 0; i < 4; ++i) {
                        if ((*param.supportedChans)[i]) {
                            if (nVals > 0)
                                OS_TemplateHeader << "| ";
                            OS_TemplateHeader << values[i];
                            ++nVals;
                        }
                    }
                    OS_TemplateHeader << ");\n";
                }
                if (!hasFlag)
                    OS_TemplateHeader << "         (void)p;\n";
                OS_TemplateHeader << "    }\n";

            } else {
                for (int i = 0; i < param.arrayDimension; ++i) {
                    OS_TemplateHeader << "    {\n";
                    std::stringstream ss;
                    ss << param.lowerCaseName;
                    if (param.arrayDimension > 1)
                        ss << "_dim_" << i;
                    std::string name = ss.str();
                    OS_TemplateHeader << "        auto p = addParameter(\"" << name << "\", getMetaTypeID<" << param.valueType << ">());\n";
                    if (param.defaultValues.empty()) {
                        OS_TemplateHeader << "        auto def = make_unique<ConstValueFunction<" << param.valueType << ">>();\n";
                        OS_TemplateHeader << "        def->setName(\"" << param.lowerCaseName << "\");\n";
                        OS_TemplateHeader << "        p->setDefaultInput(std::move(def));\n";
                    } else {
                        assert(param.defaultValues.size() == (int)param.arrayDimension);
                        OS_TemplateHeader << "        auto def = make_unique<ConstValueFunction<" << param.valueType << ">>();\n";
                        OS_TemplateHeader << "        def->setName(\"" << name << "\");\n";
                        OS_TemplateHeader << "        def->setValue(" << param.defaultValues[i] << ");\n";
                        OS_TemplateHeader << "        p->setDefaultInput(std::move(def));\n";
                    }
                    OS_TemplateHeader << "    }\n";
                    
                }

                if (param.arrayDimension > 1) {
                    OS_TemplateHeader << "        addParameter(\"" << param.lowerCaseName << "\", getMetaTypeID<def::array<" << param.valueType << ", " << param.arrayDimension << ">>());\n";

                }

            }
            
        }
        
    }
    
    
    OS_TemplateHeader << "}\n\n";
    
    for (std::size_t i = 0; i < CDef->TimeFunctionParams.size(); ++i) {
        auto& p = CDef->TimeFunctionParams[i];
        OS_TemplateHeader << TemplatePrefix << "const char* " << QualName << "::kParam_" << p.lowerCaseName << " = \"" << p.lowerCaseName << "\";\n\n";
        
    }
    
} // GenerateTimeFunctionData

void Generator::GenerateMetaCall()
{
    OS_TemplateHeader << "\n" << TemplatePrefix << "int " << QualName
        << "::qt_metacall(QMetaObject::Call _c, int _id, void **_a)\n{\n";
    if (!qBaseName.empty()) {
        OS_TemplateHeader << "    _id = " << qBaseName << "::qt_metacall(_c, _id, _a);\n";
        if (QtMethodCount || CDef->QtProperties.size()) {
            OS_TemplateHeader << "    if (_id < 0)\n"
                                 "        return _id;\n";
        }
    }

    if (QtMethodCount) {
        OS_TemplateHeader <<
              "    if (_c == QMetaObject::InvokeMetaMethod || _c == QMetaObject::RegisterMethodArgumentMetaType) {\n"
              "        if (_id < " << QtMethodCount << ")\n"
              "            qt_static_metacall(this, _c, _id, _a);\n"
              "        _id -= " << QtMethodCount << ";\n"
              "    }\n";
    }

    if (CDef->QtProperties.size()) {
        bool needDesignable = false;
        bool needScriptable = false;
        bool needStored = false;
        bool needEditable = false;
        bool needUser = false;
        for (const PropertyDef &p : CDef->QtProperties) {
            auto IsFunction = [](const std::string &S) { return S.size() && S[S.size()-1] == ')'; };
            needDesignable |= IsFunction(p.designable);
            needScriptable |= IsFunction(p.scriptable);
            needStored |= IsFunction(p.stored);
            needEditable |= IsFunction(p.editable);
            needUser |= IsFunction(p.user);
        }

        OS_TemplateHeader << "    ";
        if (QtMethodCount)
            OS_TemplateHeader << "else ";

        OS_TemplateHeader << " if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty\n"
              "            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {\n"
              "        if (_id < " << CDef->QtProperties.size() <<  ")\n"
              "            qt_static_metacall(this, _c, _id, _a);\n"
              "        _id -= " << CDef->QtProperties.size() <<  ";\n"
              "    }\n";

        // Helper for all the QMetaObject::QueryProperty*
        typedef std::string (PropertyDef::*Accessor);
        auto HandleQueryPropertyAction = [&](bool Need, const char *Action, Accessor A) {
            OS_TemplateHeader << " else ";
            OS_TemplateHeader << "if (_c == QMetaObject::" << Action << ") {\n";
            if (Need) {
                OS_TemplateHeader << "        switch (_id) {\n";
                int I = 0;
                for (const PropertyDef &p : CDef->QtProperties) {
                    OS_TemplateHeader << "        case " << (I++) <<": ";
                    const std::string &S = (p.*A);
                    if (!S.empty() && S[S.size()-1] == ')')
                        OS_TemplateHeader << "*reinterpret_cast<bool*>(_a[0]) = " << S << "; ";
                    OS_TemplateHeader << "break;\n";
                }
                OS_TemplateHeader << "        default: break;\n";
                OS_TemplateHeader << "        }";
            }
            OS_TemplateHeader << "        _id -= " << CDef->QtProperties.size() << ";\n    }";
        };

        HandleQueryPropertyAction(needDesignable, "QueryPropertyDesignable", &PropertyDef::designable);
        HandleQueryPropertyAction(needScriptable, "QueryPropertyScriptable", &PropertyDef::scriptable);
        HandleQueryPropertyAction(needStored, "QueryPropertyStored", &PropertyDef::stored);
        HandleQueryPropertyAction(needEditable, "QueryPropertyEditable", &PropertyDef::editable);
        HandleQueryPropertyAction(needUser, "QueryPropertyUser", &PropertyDef::user);

    }
    OS_TemplateHeader << "\n    return _id;\n"
          "}\n";
} // GenerateMetaCall

void Generator::GenerateBlxMetaCall()
{
    OS_TemplateHeader << "\n" << TemplatePrefix << "int " << QualName
        << "::blx_metacall(BlxMetaObject::CallType _c, int _id, void **_a)\n{\n";
    
    if (blxBaseName.empty() && BlxMethodCount == 0 && CDef->BlxProperties.empty()) {
        OS_TemplateHeader << "    Q_UNUSED(_c);  Q_UNUSED(_a);\n";
    }
    if (!blxBaseName.empty()) {
        OS_TemplateHeader << "    _id = " << blxBaseName << "::blx_metacall(_c, _id, _a);\n";
        if (BlxMethodCount || CDef->BlxProperties.size()) {
            OS_TemplateHeader << "    if (_id < 0)\n"
                                 "        return _id;\n";
        }
    }

    if (BlxMethodCount) {
        OS_TemplateHeader <<
              "    if (_c == BlxMetaObject::CallType::InvokeMethod || _c == BlxMetaObject::CallType::RegisterMethodArgumentMetaType) {\n"
              "        if (_id < " << BlxMethodCount << ")\n"
              "            blx_static_metacall(this, _c, _id, _a);\n"
              "        _id -= " << BlxMethodCount << ";\n"
              "    }\n";
    }

    if (CDef->BlxProperties.size()) {
        bool needDesignable = false;
        bool needScriptable = false;
        bool needStored = false;
        bool needEditable = false;
        bool needUser = false;
        for (const PropertyDef &p : CDef->BlxProperties) {
            auto IsFunction = [](const std::string &S) { return S.size() && S[S.size()-1] == ')'; };
            needDesignable |= IsFunction(p.designable);
            needScriptable |= IsFunction(p.scriptable);
            needStored |= IsFunction(p.stored);
            needEditable |= IsFunction(p.editable);
            needUser |= IsFunction(p.user);
        }

        OS_TemplateHeader << "    ";
        if (BlxMethodCount)
            OS_TemplateHeader << "else ";

        OS_TemplateHeader << " if (_c == BlxMetaObject::CallType::ReadProperty || _c == BlxMetaObject::CallType::WriteProperty\n"
              "            || _c == BlxMetaObject::CallType::ResetProperty || _c == BlxMetaObject::CallType::RegisterPropertyMetaType) {\n"
              "        if (_id < " << CDef->BlxProperties.size() <<  ")\n"
              "            blx_static_metacall(this, _c, _id, _a);\n"
              "        _id -= " << CDef->BlxProperties.size() <<  ";\n"
              "    }\n";

#if 0
        // Helper for all the BlxMetaObject::QueryProperty*
        typedef std::string (PropertyDef::*Accessor);
        auto HandleQueryPropertyAction = [&](bool Need, const char *Action, Accessor A) {
            OS_TemplateHeader << " else ";
            OS_TemplateHeader << "if (_c == BlxMetaObject::CallType::" << Action << ") {\n";
            if (Need) {
                OS_TemplateHeader << "        switch (_id) {\n";
                int I = 0;
                for (const PropertyDef &p : CDef->BlxProperties) {
                    OS_TemplateHeader << "        case " << (I++) <<": ";
                    const std::string &S = (p.*A);
                    if (!S.empty() && S[S.size()-1] == ')')
                        OS_TemplateHeader << "*reinterpret_cast<bool*>(_a[0]) = " << S << "; ";
                    OS_TemplateHeader << "break;\n";
                }
                OS_TemplateHeader << "        default: break;\n";
                OS_TemplateHeader << "        }";
            }
            OS_TemplateHeader << "        _id -= " << CDef->BlxProperties.size() << ";\n    }";
        };

        HandleQueryPropertyAction(needDesignable, "QueryPropertyDesignable", &PropertyDef::designable);
        HandleQueryPropertyAction(needScriptable, "QueryPropertyScriptable", &PropertyDef::scriptable);
        HandleQueryPropertyAction(needStored, "QueryPropertyStored", &PropertyDef::stored);
        HandleQueryPropertyAction(needEditable, "QueryPropertyEditable", &PropertyDef::editable);
        HandleQueryPropertyAction(needUser, "QueryPropertyUser", &PropertyDef::user);
#endif
    } // !properties.empty()
    OS_TemplateHeader << "\n    return _id;\n"
          "}\n";
} // GenerateBlxMetaCall

void Generator::GenerateStaticMetaCall()
{
    llvm::StringRef ClassName = CDef->Record->getName();
    OS_TemplateHeader << "\n" <<  TemplatePrefix << "void " << QualName
        << "::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)\n{\n    ";
    bool NeedElse = false;

    if (!CDef->QtConstructors.empty()) {
        OS_TemplateHeader << "    if (_c == QMetaObject::CreateInstance) {\n"
              "        switch (_id) {\n";

        llvm::StringRef resultType = CDef->HasQObject ? "QObject" : "void";

        int CtorIndex = 0;
        ForEachMethod(CDef->QtConstructors, [&](const clang::CXXConstructorDecl *MD, int C) {
            OS_TemplateHeader << "        case " << (CtorIndex++) << ": { " << resultType << " *_r = new " << ClassName << "(";

            for (unsigned int j = 0 ; j < MD->getNumParams() - C; ++j) {
                if (j) OS_TemplateHeader << ",";
                if (j == MD->getNumParams() - 1 && HasPrivateSignal(MD))
                    OS_TemplateHeader << "QPrivateSignal()";
                else
                    OS_TemplateHeader << "*reinterpret_cast< " << Ctx.getPointerType(MD->getParamDecl(j)->getType().getNonReferenceType().getUnqualifiedType()).getAsString(PrintPolicy) << " >(_a[" << (j+1) << "])";
            }
            OS_TemplateHeader << ");\n            if (_a[0]) *reinterpret_cast<" << resultType << "**>(_a[0]) = _r; } break;\n";
        });
        OS_TemplateHeader << "        default: break;\n"
              "        }\n"
              "    }";

        NeedElse = true;
    }

    if (QtMethodCount) {
        if(NeedElse) OS_TemplateHeader << " else ";
        NeedElse = true;
        OS_TemplateHeader << "if (_c == QMetaObject::InvokeMetaMethod) {\n";
        if (CDef->HasQObject) {
//          OS_TemplateHeader << "        Q_ASSERT(staticMetaObject.cast(_o));\n";
            OS_TemplateHeader << "        " << ClassName <<" *_t = static_cast<" << ClassName << " *>(_o);\n";
        } else {
            OS_TemplateHeader << "        " << ClassName <<" *_t = reinterpret_cast<" << ClassName << " *>(_o);\n";
        }

        OS_TemplateHeader << "        switch(_id) {\n" ;
        int MethodIndex = 0;
        auto GenerateInvokeMethod = [&](const clang::CXXMethodDecl *MD, int Clone) {
            if (!MD->getIdentifier())
                return;

            OS_TemplateHeader << "        case " << MethodIndex << ": ";
            MethodIndex++;

            if (WorkaroundTests(ClassName, MD, OS))
                return;

            auto ReturnType = getResultType(MD);
            
            // Original moc don't support reference as return type: see  Moc::parseFunction
            bool IsVoid = ReturnType->isVoidType() || ReturnType->isReferenceType();
            // If we have a decltype, we need to use auto type deduction
            bool AutoType = llvm::isa<clang::DecltypeType>(ReturnType) || llvm::isa<clang::AutoType>(ReturnType);
            if (!IsVoid) {
                OS_TemplateHeader << "{ ";
                if (AutoType)
                    OS_TemplateHeader << "auto";
                else
                    ReturnType.getUnqualifiedType().print(OS, PrintPolicy);
                OS_TemplateHeader << " _r =  ";
            }

            OS_TemplateHeader << "_t->" << MD->getName() << "(";

            for (unsigned int j = 0 ; j < MD->getNumParams() - Clone; ++j) {
                if (j) OS_TemplateHeader << ",";
                if (j == MD->getNumParams() - 1 && HasPrivateSignal(MD))
                    OS_TemplateHeader << "QPrivateSignal()";
                else
                    OS_TemplateHeader << "*reinterpret_cast< " << Ctx.getPointerType(MD->getParamDecl(j)->getType().getNonReferenceType().getUnqualifiedType()).getAsString(PrintPolicy) << " >(_a[" << (j+1) << "])";
            }
            OS_TemplateHeader << ");";
            if (!IsVoid) {
                OS_TemplateHeader << "\n            if (_a[0]) *reinterpret_cast< ";
                if (AutoType)
                    OS_TemplateHeader << "decltype(&_r)";
                else
                    Ctx.getPointerType(ReturnType.getNonReferenceType().getUnqualifiedType()).print(OS, PrintPolicy);
                OS_TemplateHeader << " >(_a[0]) = qMove(_r); }";
            }
            OS_TemplateHeader <<  " break;\n";
        };
        ForEachMethod(CDef->QtSignals, GenerateInvokeMethod);
        ForEachMethod(CDef->Slots, GenerateInvokeMethod);
        for (const PrivateSlotDef &P : CDef->PrivateSlots) {
            for (int Clone = 0; Clone <= P.NumDefault; ++Clone) {
                OS_TemplateHeader << "        case " << MethodIndex << ": ";
                // Original moc don't support reference as return type: see  Moc::parseFunction
                bool IsVoid = P.ReturnType == "void" || P.ReturnType.empty() || P.ReturnType.back() == '&';
                if (!IsVoid)
                    OS_TemplateHeader << "{ " << P.ReturnType << " _r =  ";
                OS_TemplateHeader << "_t->" << P.InPrivateClass << "->" << P.Name << "(";
                for (unsigned int j = 0 ; j < P.Args.size() - Clone; ++j) {
                    if (j) OS_TemplateHeader << ",";
                    OS_TemplateHeader << "*reinterpret_cast< " << P.Args[j] << " *>(_a[" << (j+1) << "])";
                }
                OS_TemplateHeader << ");";
                if (!IsVoid) {
                    OS_TemplateHeader << "\n            if (_a[0]) *reinterpret_cast< " << P.ReturnType << " *>(_a[0]) = _r; }";
                }
                OS_TemplateHeader <<  " break;\n";
                MethodIndex++;
            }
        }
        ForEachMethod(CDef->QtMethods, GenerateInvokeMethod);
        OS_TemplateHeader << "        default: break;\n"
              "        }\n"
              "    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {\n"
              "        switch ((_id << 16) | *reinterpret_cast<int*>(_a[1])) {\n"
              "        default: *reinterpret_cast<int*>(_a[0]) = -1; break;\n";


        MethodIndex = 0;
        auto GenerateRegisterMethodArguments = [&](const clang::CXXMethodDecl *MD, int Clone) {
            if (!MD->getIdentifier()) {
                MethodIndex++;
                return;
            }
          //  RegisterT(getResultType(MD), (MethodIndex << 16));
            int argc = MD->getNumParams() - Clone - (HasPrivateSignal(MD)?1:0);
            for (int j = 0 ; j < argc ; ++j) {
                auto Type = MD->getParamDecl(j)->getType();
                if (!Moc->ShouldRegisterMetaType(Type))
                    break;
                OS_TemplateHeader << "       case 0x";
                OS_TemplateHeader.write_hex((MethodIndex << 16) | j);
                OS_TemplateHeader << ": *reinterpret_cast<int*>(_a[0]) = ";
                OS_TemplateHeader <<  "QtPrivate::QMetaTypeIdHelper< " << Type.getNonReferenceType().getUnqualifiedType().getAsString(PrintPolicy)
                    << " >::qt_metatype_id(); break;\n";
            }
            MethodIndex++;
        };

        ForEachMethod(CDef->QtSignals, GenerateRegisterMethodArguments);
        ForEachMethod(CDef->Slots, GenerateRegisterMethodArguments);
        MethodIndex += CDef->PrivateSlotCount; // TODO: we should also register these types.
        ForEachMethod(CDef->QtMethods, GenerateRegisterMethodArguments);

        OS_TemplateHeader << "        }\n    }";

    }
    if (!CDef->QtSignals.empty()) {

        int MethodIndex = 0;
        OS_TemplateHeader << " else if (_c == QMetaObject::IndexOfMethod) {\n"
              "        int *result = reinterpret_cast<int *>(_a[0]);\n"
              "        void **func = reinterpret_cast<void **>(_a[1]);\n";

        for (const clang::CXXMethodDecl *MD: CDef->QtSignals) {
            int Idx = MethodIndex;
            MethodIndex += MD->getNumParams() - MD->getMinRequiredArguments() + 1;
            if (MD->isStatic() || !MD->getIdentifier())
                continue;
            OS_TemplateHeader << "        {\n"
                  "            typedef " << getResultType(MD).getAsString(PrintPolicy) << " (" << ClassName << "::*_t)(";
            for (unsigned int j = 0 ; j < MD->getNumParams(); ++j) {
                if (j) OS_TemplateHeader << ",";
                OS_TemplateHeader << MD->getParamDecl(j)->getType().getAsString(PrintPolicy);
            }
            if (MD->isConst()) OS_TemplateHeader << ") const;\n";
            else OS_TemplateHeader << ");\n";

            OS_TemplateHeader <<
                  "            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&"<< ClassName <<"::"<< MD->getName() <<")) {\n"
                  "                *result = " << Idx << ";\n"
                  "            }\n"
                  "        }\n";
        }
        OS_TemplateHeader << "    }";
    }

    if (!CDef->QtProperties.empty()) {
        if(NeedElse) OS_TemplateHeader << " else ";
        NeedElse = true;

        bool needGet = false;
        //bool needTempVarForGet = false;
        bool needSet = false;
        bool needReset = false;
        for (const PropertyDef &p : CDef->QtProperties) {
            needGet |= !p.read.empty() || !p.member.empty();
            /*if (!p.read.empty())
                needTempVarForGet |= (p.gspec != PropertyDef::PointerSpec
                && p.gspec != PropertyDef::ReferenceSpec);*/
            needSet |= !p.write.empty() || (!p.member.empty() && !p.constant);
            needReset |= !p.reset.empty();
        }


        // Generate the code for QMetaObject::'Action'.  calls 'Functor' to generate the  code for
        // each properties
        auto HandlePropertyAction = [&](bool Need, const char *Action,
                                        const std::function<void(const PropertyDef &)> &Functor) {
            OS_TemplateHeader << "if (_c == QMetaObject::" << Action << ") {\n";
            if (Need) {
                if (CDef->HasQObject) {
                    // OS_TemplateHeader << "        Q_ASSERT(staticMetaObject.cast(_o));\n";
                    OS_TemplateHeader << "        " << ClassName <<" *_t = static_cast<" << ClassName << " *>(_o);\n";
                } else {
                    OS_TemplateHeader << "        " << ClassName <<" *_t = reinterpret_cast<" << ClassName << " *>(_o);\n";
                }
                OS_TemplateHeader << "        switch (_id) {\n";
                int I = 0;
                for (const PropertyDef &p : CDef->QtProperties) {
                    OS_TemplateHeader << "        case " << (I++) <<": ";
                    Functor(p);
                    OS_TemplateHeader << "break;\n";
                }
                OS_TemplateHeader << "        default: break;\n";
                OS_TemplateHeader << "        }";
            }
            OS_TemplateHeader << "        _id -= " << CDef->QtProperties.size() << ";\n    }";
        };

        HandlePropertyAction(needGet, "ReadProperty", [&](const PropertyDef &p) {
            if (p.read.empty() && p.member.empty())
                return;

            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }

            //FIXME: enums case
            if (p.PointerHack) {
              OS_TemplateHeader << "_a[0] = const_cast<void*>(static_cast<const void*>(" << Prefix << p.read << "())); ";
            } else {
              OS_TemplateHeader << "*reinterpret_cast< " << p.type << "*>(_a[0]) = " << Prefix;
              if (!p.read.empty())
                  OS_TemplateHeader << p.read << "(); ";
              else
                  OS_TemplateHeader << p.member << "; ";
            }
        });
        OS_TemplateHeader << " else ";
        HandlePropertyAction(needSet, "WriteProperty", [&](const PropertyDef &p) {
            if (p.constant)
                return;

            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }

            if (!p.write.empty()) {
                OS_TemplateHeader << Prefix << p.write << "(*reinterpret_cast< " << p.type << "*>(_a[0])); ";
            } else if (!p.member.empty()) {
                std::string M = Prefix + p.member;
                std::string A = "*reinterpret_cast< " + p.type + "*>(_a[0])";
                if (!p.notify.Str.empty()) {
                    OS_TemplateHeader << "\n"
                          "            if (" << M << " != " << A << ") {\n"
                          "                " << M << " = " << A << ";\n"
                          "                Q_EMIT _t->" << p.notify.Str << "(";
                    if (p.notify.MD && p.notify.MD->getMinRequiredArguments() > 0)
                        OS_TemplateHeader << M;
                    OS_TemplateHeader << ");\n"
                          "            } ";
                } else {
                    OS_TemplateHeader << M << " = " << A << "; ";
                }
            }
        });
        OS_TemplateHeader << " else ";
        HandlePropertyAction(needReset, "ResetProperty", [&](const PropertyDef &p) {
            if (p.reset.empty() || p.reset[p.reset.size()-1] != ')')
                return;
            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }
            OS_TemplateHeader << Prefix << p.reset << "; ";
        });

        OS_TemplateHeader << "if (_c == QMetaObject::RegisterPropertyMetaType) {\n"
              "        switch (_id) {\n"
              "        default: *reinterpret_cast<int*>(_a[0]) = -1; break;\n";

        //FIXME: optimize (group same properties, and don't generate for builtin
        int Idx = 0;
        for (const PropertyDef &P: CDef->QtProperties) {
            int OldIdx = Idx++;
            if (P.PossiblyForwardDeclared) {
                const auto &MTS = Moc->registered_meta_type;
                if (!std::any_of(MTS.begin(), MTS.end(), [&](const clang::Type*T){
                    return clang::QualType(T,0).getAsString(PrintPolicy) == P.type;
                } ))
                    continue;
            }
            OS_TemplateHeader << "        case " << OldIdx << ": *reinterpret_cast<int*>(_a[0]) = QtPrivate::QMetaTypeIdHelper<"
               << P.type << " >::qt_metatype_id(); break;\n";
        }
        OS_TemplateHeader << "        }\n";
        OS_TemplateHeader << "    }\n";
    }


    OS_TemplateHeader << "\n    Q_UNUSED(_o); Q_UNUSED(_id); Q_UNUSED(_c); Q_UNUSED(_a);";
    OS_TemplateHeader << "\n}\n";
} // GenerateStaticMetaCall


void Generator::GenerateBlxStaticMetaCall()
{
    llvm::StringRef ClassName = CDef->Record->getName();
    OS_TemplateHeader << "\n" <<  TemplatePrefix << "void " << QualName
        << "::blx_static_metacall(void *_o, BlxMetaObject::CallType _c, int _id, void **_a)\n{\n    ";
    bool NeedElse = false;

    if (!CDef->BlxConstructors.empty()) {
        OS_TemplateHeader << "    if (_c == BlxMetaObject::CallType::CreateInstance) {\n"
              "        switch (_id) {\n";

        llvm::StringRef resultType = CDef->HasBlxClass ? "BlxObject" : "void";

        int CtorIndex = 0;
        ForEachMethod(CDef->BlxConstructors, [&](const clang::CXXConstructorDecl *MD, int C) {
            OS_TemplateHeader << "        case " << (CtorIndex++) << ": { " << resultType << " *_r = new " << ClassName << "(";

            for (unsigned int j = 0 ; j < MD->getNumParams() - C; ++j) {
                if (j) OS_TemplateHeader << ",";
#if 0
                if (j == MD->getNumParams() - 1 && HasPrivateSignal(MD))
                    OS_TemplateHeader << "QPrivateSignal()";
                else
#endif
                    OS_TemplateHeader << "*reinterpret_cast< " << Ctx.getPointerType(MD->getParamDecl(j)->getType().getNonReferenceType().getUnqualifiedType()).getAsString(PrintPolicy) << " >(_a[" << (j+1) << "])";
            }
            OS_TemplateHeader << ");\n            if (_a[0]) *reinterpret_cast<" << resultType << "**>(_a[0]) = _r; } break;\n";
        });
        OS_TemplateHeader << "        default: break;\n"
              "        }\n"
              "    }";

        NeedElse = true;
    }

    if (BlxMethodCount) {
        if(NeedElse) OS_TemplateHeader << " else ";
        NeedElse = true;
        OS_TemplateHeader << "if (_c == BlxMetaObject::CallType::InvokeMethod) {\n";
        if (CDef->HasBlxClass) {
//          OS_TemplateHeader << "        Q_ASSERT(staticMetaObject.cast(_o));\n";
            OS_TemplateHeader << "        " << ClassName <<" *_t = static_cast<" << ClassName << " *>(_o);\n";
        } else {
            OS_TemplateHeader << "        " << ClassName <<" *_t = reinterpret_cast<" << ClassName << " *>(_o);\n";
        }

        OS_TemplateHeader << "        switch(_id) {\n" ;
        int MethodIndex = 0;
        auto GenerateInvokeMethod = [&](const clang::CXXMethodDecl *MD, int Clone) {
            //if (!MD->getIdentifier())
            //    return;

            OS_TemplateHeader << "        case " << MethodIndex << ": ";
            MethodIndex++;

            if (WorkaroundTests(ClassName, MD, OS))
                return;

            auto ReturnType = getResultType(MD);
            
            // Original moc don't support reference as return type: see  Moc::parseFunction
            bool IsVoid = ReturnType->isVoidType() || ReturnType->isReferenceType();
            // If we have a decltype, we need to use auto type deduction
            bool AutoType = llvm::isa<clang::DecltypeType>(ReturnType) || llvm::isa<clang::AutoType>(ReturnType);
            if (!IsVoid) {
                OS_TemplateHeader << "{ ";
                if (AutoType) {
                    OS_TemplateHeader << "auto";
                } else {
                    ReturnType.getUnqualifiedType().print(OS, PrintPolicy);
                }
                OS_TemplateHeader << " _r =  ";
            }

            OS_TemplateHeader << "_t->" << MD->getDeclName().getAsString() << "(";

            for (unsigned int j = 0 ; j < MD->getNumParams() - Clone; ++j) {
                if (j) OS_TemplateHeader << ",";
#if 0
                if (j == MD->getNumParams() - 1 && HasPrivateSignal(MD))
                    OS_TemplateHeader << "QPrivateSignal()";
                else
#endif
                    OS_TemplateHeader << "*reinterpret_cast< " << Ctx.getPointerType(MD->getParamDecl(j)->getType().getNonReferenceType().getUnqualifiedType()).getAsString(PrintPolicy) << " >(_a[" << (j+1) << "])";
            }
            OS_TemplateHeader << ");";
            if (!IsVoid) {
                OS_TemplateHeader << "\n            if (_a[0]) *reinterpret_cast< ";
                if (AutoType)
                    OS_TemplateHeader << "decltype(&_r)";
                else
                    Ctx.getPointerType(ReturnType.getNonReferenceType().getUnqualifiedType()).print(OS, PrintPolicy);
                if (!ReturnType->isReferenceType())
                    OS_TemplateHeader << " >(_a[0]) = std::move(_r); }";
                else
                    OS_TemplateHeader << " >(_a[0]) = &_r; }";
            }
            OS_TemplateHeader <<  " break;\n";
        };
        //ForEachMethod(CDef->Signals, GenerateInvokeMethod);
        //ForEachMethod(CDef->Slots, GenerateInvokeMethod);
#if 0
        for (const PrivateSlotDef &P : CDef->PrivateSlots) {
            for (int Clone = 0; Clone <= P.NumDefault; ++Clone) {
                OS_TemplateHeader << "        case " << MethodIndex << ": ";
                // Original moc don't support reference as return type: see  Moc::parseFunction
                bool IsVoid = P.ReturnType == "void" || P.ReturnType.empty() || P.ReturnType.back() == '&';
                if (!IsVoid)
                    OS_TemplateHeader << "{ " << P.ReturnType << " _r =  ";
                OS_TemplateHeader << "_t->" << P.InPrivateClass << "->" << P.Name << "(";
                for (unsigned int j = 0 ; j < P.Args.size() - Clone; ++j) {
                    if (j) OS_TemplateHeader << ",";
                    OS_TemplateHeader << "*reinterpret_cast< " << P.Args[j] << " *>(_a[" << (j+1) << "])";
                }
                OS_TemplateHeader << ");";
                if (!IsVoid) {
                    OS_TemplateHeader << "\n            if (_a[0]) *reinterpret_cast< " << P.ReturnType << " *>(_a[0]) = _r; }";
                }
                OS_TemplateHeader <<  " break;\n";
                MethodIndex++;
            }
        }
#endif
        ForEachMethod(CDef->BlxMethods, GenerateInvokeMethod);
        OS_TemplateHeader << "        default: break;\n"
              "        }\n"
              "    } else if (_c == BlxMetaObject::CallType::RegisterMethodArgumentMetaType) {\n"
              "        switch ((_id << 16) | *reinterpret_cast<int*>(_a[1])) {\n"
              "        default: *reinterpret_cast<BlxMetaTypeID*>(_a[0]) = BlxMetaTypeID::InvalidType; break;\n";


        MethodIndex = 0;
        auto GenerateRegisterMethodArguments = [&](const clang::CXXMethodDecl *MD, int Clone) {
            if (!MD->getIdentifier()) {
                MethodIndex++;
                return;
            }
          //  RegisterT(getResultType(MD), (MethodIndex << 16));
            int argc = MD->getNumParams() - Clone - (HasPrivateSignal(MD)?1:0);
            for (int j = 0 ; j < argc ; ++j) {
                auto Type = MD->getParamDecl(j)->getType();
                if (!Moc->ShouldRegisterMetaType(Type))
                    break;
                OS_TemplateHeader << "       case 0x";
                OS_TemplateHeader.write_hex((MethodIndex << 16) | j);
                OS_TemplateHeader << ": *reinterpret_cast<BlxMetaTypeID*>(_a[0]) = ";
                OS_TemplateHeader <<  "details::BlxMetaTypeIdHelper< " << Type.getNonReferenceType().getUnqualifiedType().getAsString(PrintPolicy)
                    << " >::getBlxMetaTypeID(); break;\n";
            }
            MethodIndex++;
        };

        //ForEachMethod(CDef->Signals, GenerateRegisterMethodArguments);
        //ForEachMethod(CDef->Slots, GenerateRegisterMethodArguments);
        //MethodIndex += CDef->PrivateSlotCount; // TODO: we should also register these types.
        ForEachMethod(CDef->BlxMethods, GenerateRegisterMethodArguments);

        OS_TemplateHeader << "        }\n    }";

    }

    if (!CDef->BlxSignals.empty()) {

        int MethodIndex = 0;
        OS_TemplateHeader << " else if (_c == BlxMetaObject::IndexOfMethod) {\n"
              "        int *result = reinterpret_cast<int *>(_a[0]);\n"
              "        void **func = reinterpret_cast<void **>(_a[1]);\n";

        for (const clang::CXXMethodDecl *MD: CDef->BlxSignals) {
            int Idx = MethodIndex;
            MethodIndex += MD->getNumParams() - MD->getMinRequiredArguments() + 1;
            if (MD->isStatic() || !MD->getIdentifier())
                continue;
            OS_TemplateHeader << "        {\n"
                  "            typedef " << getResultType(MD).getAsString(PrintPolicy) << " (" << ClassName << "::*_t)(";
            for (unsigned int j = 0 ; j < MD->getNumParams(); ++j) {
                if (j) OS_TemplateHeader << ",";
                OS_TemplateHeader << MD->getParamDecl(j)->getType().getAsString(PrintPolicy);
            }
            if (MD->isConst()) OS_TemplateHeader << ") const;\n";
            else OS_TemplateHeader << ");\n";

            OS_TemplateHeader <<
                  "            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&"<< ClassName <<"::"<< MD->getName() <<")) {\n"
                  "                *result = " << Idx << ";\n"
                  "            }\n"
                  "        }\n";
        }
        OS_TemplateHeader << "    }";
    }

    if (!CDef->BlxProperties.empty()) {
        if(NeedElse) OS_TemplateHeader << " else ";
        NeedElse = true;

        bool needGet = false;
        //bool needTempVarForGet = false;
        bool needSet = false;
        bool needReset = false;
        for (const PropertyDef &p : CDef->BlxProperties) {
            needGet |= !p.read.empty() || !p.member.empty();
            /*if (!p.read.empty())
                needTempVarForGet |= (p.gspec != PropertyDef::PointerSpec
                && p.gspec != PropertyDef::ReferenceSpec);*/
            needSet |= !p.write.empty() || (!p.member.empty() && !p.constant);
            needReset |= !p.reset.empty();
        }


        // Generate the code for QMetaObject::'Action'.  calls 'Functor' to generate the  code for
        // each properties
        auto HandlePropertyAction = [&](bool Need, const char *Action,
                                        const std::function<void(const PropertyDef &)> &Functor) {
            OS_TemplateHeader << "if (_c == BlxMetaObject::CallType::" << Action << ") {\n";
            if (Need) {
                if (CDef->HasBlxClass) {
                    // OS_TemplateHeader << "        Q_ASSERT(staticMetaObject.cast(_o));\n";
                    OS_TemplateHeader << "        " << ClassName <<" *_t = static_cast<" << ClassName << " *>(_o);\n";
                } else {
                    OS_TemplateHeader << "        " << ClassName <<" *_t = reinterpret_cast<" << ClassName << " *>(_o);\n";
                }
                OS_TemplateHeader << "        switch (_id) {\n";
                int I = 0;
                for (const PropertyDef &p : CDef->BlxProperties) {
                    OS_TemplateHeader << "        case " << (I++) <<": ";
                    Functor(p);
                    OS_TemplateHeader << "break;\n";
                }
                OS_TemplateHeader << "        default: break;\n";
                OS_TemplateHeader << "        }";
            }
            OS_TemplateHeader << "        _id -= " << CDef->BlxProperties.size() << ";\n    }";
        };

        HandlePropertyAction(needGet, "ReadProperty", [&](const PropertyDef &p) {
            if (p.read.empty() && p.member.empty())
                return;

            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }

            //FIXME: enums case
            if (p.PointerHack) {
              OS_TemplateHeader << "_a[0] = const_cast<void*>(static_cast<const void*>(" << Prefix << p.read << "())); ";
            } else {
              OS_TemplateHeader << "*reinterpret_cast< " << p.type << "*>(_a[0]) = " << Prefix;
              if (!p.read.empty())
                  OS_TemplateHeader << p.read << "(); ";
              else
                  OS_TemplateHeader << p.member << "; ";
            }
        });
        OS_TemplateHeader << " else ";
        HandlePropertyAction(needSet, "WriteProperty", [&](const PropertyDef &p) {
            if (p.constant)
                return;

            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }

            if (!p.write.empty()) {
                OS_TemplateHeader << Prefix << p.write << "(*reinterpret_cast< " << p.type << "*>(_a[0])); ";
            } else if (!p.member.empty()) {
                std::string M = Prefix + p.member;
                std::string A = "*reinterpret_cast< " + p.type + "*>(_a[0])";
                if (!p.notify.Str.empty()) {
                    OS_TemplateHeader << "\n"
                          "            if (" << M << " != " << A << ") {\n"
                          "                " << M << " = " << A << ";\n"
                          "                Q_EMIT _t->" << p.notify.Str << "(";
                    if (p.notify.MD && p.notify.MD->getMinRequiredArguments() > 0)
                        OS_TemplateHeader << M;
                    OS_TemplateHeader << ");\n"
                          "            } ";
                } else {
                    OS_TemplateHeader << M << " = " << A << "; ";
                }
            }
        });
        OS_TemplateHeader << " else ";
        HandlePropertyAction(needReset, "ResetProperty", [&](const PropertyDef &p) {
            if (p.reset.empty() || p.reset[p.reset.size()-1] != ')')
                return;
            std::string Prefix = "_t->";
            if (p.inPrivateClass.size()) {
                Prefix += p.inPrivateClass;
                Prefix += "->";
            }
            OS_TemplateHeader << Prefix << p.reset << "; ";
        });

        OS_TemplateHeader << "if (_c == BlxMetaObject::CallType::RegisterPropertyMetaType) {\n"
              "        switch (_id) {\n"
              "        default: *reinterpret_cast<BlxMetaTypeID*>(_a[0]) = BlxMetaTypeID::InvalidType; break;\n";

        //FIXME: optimize (group same properties, and don't generate for builtin
        int Idx = 0;
        for (const PropertyDef &P: CDef->BlxProperties) {
            int OldIdx = Idx++;
            if (P.PossiblyForwardDeclared) {
                const auto &MTS = Moc->registered_meta_type;
                if (!std::any_of(MTS.begin(), MTS.end(), [&](const clang::Type*T){
                    return clang::QualType(T,0).getAsString(PrintPolicy) == P.type;
                } ))
                    continue;
            }
            OS_TemplateHeader << "        case " << OldIdx << ": *reinterpret_cast<BlxMetaTypeID*>(_a[0]) = details::BlxMetaTypeIdHelper<"
               << P.type << " >::getBlxMetaTypeID(); break;\n";
        }
        OS_TemplateHeader << "        }\n";
        OS_TemplateHeader << "    }\n";
    }


    OS_TemplateHeader << "\n    Q_UNUSED(_o); Q_UNUSED(_id); Q_UNUSED(_c); Q_UNUSED(_a);";
    OS_TemplateHeader << "\n}\n";
} // GenerateBlxStaticMetaCall


void Generator::GenerateSignal(const clang::CXXMethodDecl *MD, int Idx)
{
    if (MD->isPure())
        return;

    clang::QualType ReturnType = getResultType(MD);
    // getResultType will desugar.  So if we are still a decltype, it probably means this is type
    // dependant and therefore must be a literal decltype which we need to have in the proper scope
    bool TrailingReturn = llvm::isa<clang::DecltypeType>(ReturnType);

    OS_TemplateHeader << "\n// SIGNAL " << Idx << "\n" << TemplatePrefix;
    if (TrailingReturn)
        OS_TemplateHeader << "auto ";
    else
        OS_TemplateHeader << ReturnType.getAsString(PrintPolicy) << " ";
    OS_TemplateHeader << QualName << "::" << MD->getName() + "(";
    for (unsigned int j = 0 ; j < MD->getNumParams(); ++j) {
        if (j) OS_TemplateHeader << ",";
        OS_TemplateHeader << MD->getParamDecl(j)->getType().getAsString(PrintPolicy);
        OS_TemplateHeader << " _t" << (j+1);;
    }
    OS_TemplateHeader << ")";
    std::string This = "this";
    if (MD->isConst()) {
        OS_TemplateHeader << " const";
        This = "const_cast< " + CDef->Record->getNameAsString()  + " *>(this)";
    }
    if (TrailingReturn)
        OS_TemplateHeader << " -> " << ReturnType.getAsString(PrintPolicy);
    OS_TemplateHeader << "\n{\n";
    bool IsVoid = ReturnType->isVoidType();
    unsigned int NumParam = MD->getNumParams();
    if (IsVoid && NumParam == 0) {
        OS_TemplateHeader << "    QMetaObject::activate(" << This << ", &staticMetaObject, " << Idx << ", 0);\n";
    } else {
        std::string T = ReturnType.getNonReferenceType().getUnqualifiedType().getAsString(PrintPolicy);
        if (ReturnType->isPointerType()) {
            OS_TemplateHeader << "    " << ReturnType.getAsString(PrintPolicy) << " _t0 = 0;\n";
        } else if (!IsVoid) {
            OS_TemplateHeader << "    " << T << " _t0 = " << T << "();\n";
        }
        OS_TemplateHeader << "    void *_a[] = { ";
        if (IsVoid) OS_TemplateHeader << "0";
        else OS_TemplateHeader << "&_t0";


        for (unsigned int j = 0 ; j < NumParam; ++j) {
            if (MD->getParamDecl(j)->getType().isVolatileQualified())
                OS_TemplateHeader << ", const_cast<void*>(reinterpret_cast<const volatile void*>(&_t" << (j+1) << "))";
            else
                OS_TemplateHeader << ", const_cast<void*>(reinterpret_cast<const void*>(&_t" << (j+1) << "))";
        }

        OS_TemplateHeader << " };\n"
              "    QMetaObject::activate(" << This << ", &staticMetaObject, " << Idx << ", _a);\n";

        if (!IsVoid)
            OS_TemplateHeader << "    return _t0;\n";
    }
    OS_TemplateHeader <<"}\n";
}

// Generate the data in the data array for the properties
void Generator::GenerateProperties()
{
    const std::vector<PropertyDef>& properties = CDef->QtProperties;
    if (properties.empty())
        return;

    OS << "\n // properties: name, type, flags\n";

    for (const PropertyDef &p : properties) {
        unsigned int flags = (int)QtPropertyFlags::Invalid;
        if (p.isEnum)
            flags |= (int)QtPropertyFlags::EnumOrFlag;
        if (!p.member.empty() && !p.constant)
            flags |= (int)QtPropertyFlags::Writable;
        if (!p.read.empty() || !p.member.empty())
            flags |= (int)QtPropertyFlags::Readable;
        if (!p.write.empty()) {
            flags |= (int)QtPropertyFlags::Writable;

            llvm::StringRef W(p.write);
            if (W.startswith("set") && W[3] == char(toupper(p.name[0])) &&  W.substr(4) == &p.name[1])
                flags |= (int)QtPropertyFlags::StdCppSet;
        }
        if (!p.reset.empty())
            flags |= (int)QtPropertyFlags::Resettable;
        if (p.designable.empty())
            flags |= (int)QtPropertyFlags::ResolveDesignable;
        else if (p.designable != "false")
            flags |= (int)QtPropertyFlags::Designable;
        if (p.scriptable.empty())
            flags |= (int)QtPropertyFlags::ResolveScriptable;
        else if (p.scriptable != "false")
            flags |= (int)QtPropertyFlags::Scriptable;
        if (p.stored.empty())
            flags |= (int)QtPropertyFlags::ResolveStored;
        else if (p.stored != "false")
            flags |= (int)QtPropertyFlags::Stored;
        if (p.editable.empty())
            flags |= (int)QtPropertyFlags::ResolveEditable;
        else if (p.editable != "false")
            flags |= (int)QtPropertyFlags::Editable;
        if (p.user.empty())
            flags |= (int)QtPropertyFlags::ResolveUser;
        else if (p.user != "false")
            flags |= (int)QtPropertyFlags::User;
        if (!p.notify.Str.empty())
            flags |= (int)QtPropertyFlags::Notify;
        if (p.revision > 0)
            flags |= (int)QtPropertyFlags::Revisioned;
        if (p.constant)
            flags |= (int)QtPropertyFlags::Constant;
        if (p.final)
            flags |= (int)QtPropertyFlags::Final;
        OS << "    " << StrIdx(p.name) << ", 0x80000000 | " << StrIdx(p.type) << ", 0x";
        OS.write_hex(flags) << ", // " << p.name << "\n";
    }

    if(CDef->QtNotifyCount) {
         OS << "\n // properties: notify_signal_id\n";
         for (const PropertyDef &P : properties) {
            if (P.notify.notifyId >= 0) {
                OS << "    " << P.notify.notifyId << ",\n";
            } else if (!P.notify.Str.empty()) {
                OS << "    0x70000000 | " << StrIdx(P.notify.Str) << ",\n";
            } else {
                OS << "    0,\n";
            }
         }
     }

     if(CDef->QtRevisionPropertyCount) {
         OS << "\n // properties: revision\n";
         for (const PropertyDef &P : properties) {
             OS << "    " << P.revision << ",\n";
         }
     }
} // GenerateProperties

void Generator::GenerateBlxProperties()
{
    const std::vector<PropertyDef>& properties = CDef->BlxProperties;
    if (properties.empty())
        return;

    OS << "\n // properties: name, type, flags\n";

    for (const PropertyDef &p : properties) {
        unsigned int flags = (unsigned int)BlxMetaPropertyFlag::Invalid;
        if (p.isEnum)
            flags |= (unsigned int)BlxMetaPropertyFlag::EnumOrFlag;
        if (!p.member.empty() && !p.constant)
            flags |= (unsigned int)BlxMetaPropertyFlag::Writable;
        if (!p.read.empty() || !p.member.empty())
            flags |= (unsigned int)BlxMetaPropertyFlag::Readable;
        if (!p.write.empty()) {
            flags |= (unsigned int)BlxMetaPropertyFlag::Writable;
#if 0
            llvm::StringRef W(p.write);
            if (W.startswith("set") && W[3] == char(toupper(p.name[0])) &&  W.substr(4) == &p.name[1])
                flags |= StdCppSet;
#endif
        }
        if (!p.reset.empty())
            flags |= (unsigned int)BlxMetaPropertyFlag::Resettable;
        
        if (!p.notify.Str.empty())
            flags |= (unsigned int)BlxMetaPropertyFlag::Notify;
        if (p.revision > 0)
            flags |= (unsigned int)BlxMetaPropertyFlag::Revisioned;
        if (p.constant)
            flags |= (unsigned int)BlxMetaPropertyFlag::Constant;
       
        OS << "    " << StrIdx(p.name) << ", 0x80000000 | " << StrIdx(p.type) << ", 0x";
        OS.write_hex(flags) << ", // " << p.name << "\n";
    }

    if(CDef->BlxNotifyCount) {
         OS << "\n // properties: notify_signal_id\n";
         for (const PropertyDef &P : properties) {
            if (P.notify.notifyId >= 0) {
                OS << "    " << P.notify.notifyId << ",\n";
            } else if (!P.notify.Str.empty()) {
                OS << "    0x70000000 | " << StrIdx(P.notify.Str) << ",\n";
            } else {
                OS << "    0,\n";
            }
         }
     }

     if(CDef->BlxRevisionPropertyCount) {
         OS << "\n // properties: revision\n";
         for (const PropertyDef &P : properties) {
             OS << "    " << P.revision << ",\n";
         }
     }

} // GenerateBlxProperties

// Generate the data in the data array for the enum.
void Generator::GenerateEnums(int EnumIndex, CodeGenType type)
{
    const std::vector<std::tuple<clang::EnumDecl*, std::string, bool>>& enums = (type == eCodeGenQt) ? Def->QtEnums : Def->BlxEnums;
    if (enums.empty())
        return;

    OS << "\n  // enums: name, alias, flags, count, data\n";

    for (auto e : enums) {
        int Count = 0;
        for (auto it = std::get<0>(e)->enumerator_begin() ; it != std::get<0>(e)->enumerator_end(); ++it)
            Count++;
        unsigned flags = 0;
        if(std::get<2>(e))
            flags |= 0x1; // EnumIsFlag
        if(std::get<0>(e)->isScoped())
            flags |= 0x2; // EnumIsScoped
        int enumNameIdx =  std::get<1>(e).empty() ? StrIdx(std::get<0>(e)->getName()) : StrIdx(std::get<1>(e));
        OS << "    " << StrIdx(std::get<0>(e)->getName()) << ", "  << enumNameIdx << ", " << flags << ", " << Count << ", " << EnumIndex << ",\n";
        EnumIndex += Count*2;
    }

    OS << "\n  // enums data: key, values\n";
    for (auto e : enums) {
        for (auto it = std::get<0>(e)->enumerator_begin() ; it != std::get<0>(e)->enumerator_end(); ++it) {
            clang::EnumConstantDecl *E = *it;
            OS << "    " << StrIdx(E->getName()) << ", std::uint64_t(" << QualName << "::";
            if (std::get<0>(e)->isScoped())
                OS << std::get<1>(e) << "::";
            OS << E->getName() <<"),\n";
        }
    }
}

// Returns the index of a string in the string data.
// Register the string if it is not yet registered.
int Generator::StrIdx(llvm::StringRef Str)
{
    std::string S = Str;
    auto It = std::find(Strings.begin(), Strings.end(), S);
    if (It != Strings.end())
        return It - Strings.begin();
    Strings.push_back(std::move(S));
    return Strings.size() - 1;
}

void Generator::GeneratePluginMetaData(bool Debug)
{
    QBJS::Value Data;
    Data.T = QBJS::Object;
    Data.Props["IID"] = CDef->Plugin.IID;
    Data.Props["className"] = CDef->Record->getNameAsString();
    Data.Props["version"] = double(QT_VERSION);
    Data.Props["MetaData"] = CDef->Plugin.MetaData;
    Data.Props["debug"] = Debug;
    for (const auto &It : MetaData) {
        QBJS::Value &Array = Data.Props[It.first];
        if (Array.T == QBJS::Undefined)
            Array.T = QBJS::Array;
        Array.Elems.push_back(std::string(It.second));
    }
    OS << "QT_PLUGIN_METADATA_SECTION\n"
          "static const unsigned char qt_pluginMetaData[] = {\n"
          "    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', ' ',\n"
          "    'q', 'b', 'j', 's', 0x1, 0, 0, 0,\n    ";
    QBJS::Stream JSON(OS);
    JSON << Data;
    OS << "\n};\n";
}
