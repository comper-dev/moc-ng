/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "stringutils.h"

// Most of the code is taken from qdir.h in QtCore
namespace StrUtils
{
    std::vector<std::string> split(const std::string &text, char sep, bool skipEmptyParts) {
        std::vector<std::string> tokens;
        std::size_t start = 0, end = 0;
        while ((end = text.find(sep, start)) != std::string::npos) {
            if (end != start) {
                std::string p = text.substr(start, end - start);
                if (!p.empty() || !skipEmptyParts) {
                    tokens.push_back(p);
                }
            }
            start = end + 1;
        }
        if (end != start) {
            std::string p = text.substr(start);
            if (!p.empty() || !skipEmptyParts) {
                tokens.push_back(p);
            }
        }
        return tokens;
    }
    

    int compareCaseInsensitive(const std::string& s1, const std::string& s2)
    {
        if (s1.size() != s2.size()) {
            if (s1.size() < s2.size()) {
                return -1;
            } else {
                return 1;
            }
        }
        for (std::size_t i = 0; i < s1.size(); ++i) {
            char c1 = toupper(s1[i]);
            char c2 = toupper(s2[i]);
            if (c1 < c2) {
                return -1;
            } else if (c1 > c2) {
                return 1;
            }
        }
        return 0;
    }
    
#if defined(_WIN32)
    inline std::string driveSpec(const std::string &path)
    {
        if (path.size() < 2)
            return std::string();
        char c = path[0];
        if ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z'))
            return std::string();
        if (path[1] != ':')
            return std::string();
        return path.substr(0, 2);
    }
#endif
    
    inline std::string tolower(const std::string& s)
    {
        std::string r;
        r.reserve(s.size());
        for (std::size_t i = 0; i < s.size(); ++i) {
            r.push_back(std::tolower(s[i]));
        }
        return r;
    }
    
    inline char separator()
    {
#if defined(_WIN32)
        return '\\';
#else
        return '/';
#endif
    }
    enum PathNormalization {
        DefaultNormalization = 0x00,
        AllowUncPaths = 0x01,
        RemotePath = 0x02
    };
    
    // Return the length of the root part of an absolute path, for use by cleanPath(), cd().
    static int rootLength(const std::string &name, bool allowUncPaths)
    {
        const int len = name.length();
        // starts with double slash
        if (allowUncPaths && startsWith(name, "//")) {
            // Server name '//server/path' is part of the prefix.
            const std::size_t nextSlash = name.find("/", 2);
            return nextSlash != std::string::npos ? nextSlash + 1 : len;
        }
#if defined(_WIN32)
        if (len >= 2 && name[1] == ':') {
            // Handle a possible drive letter
            return len > 2 && name[2] == '/' ? 3 : 2;
        }
#endif
        if (name[0] == '/')
            return 1;
        return 0;
    }
    
    /*!
     \internal
     Returns \a path with redundant directory separators removed,
     and "."s and ".."s resolved (as far as possible).
     This method is shared with QUrl, so it doesn't deal with QDir::separator(),
     nor does it remove the trailing slash, if any.
     */
    static std::string qt_normalizePathSegments(const std::string &name, int flags, bool *ok)
    {
        const bool allowUncPaths = AllowUncPaths & flags;
        const bool isRemote = RemotePath & flags;
        const int len = name.length();
        if (ok)
            *ok = false;
        if (len == 0)
            return name;
        int i = len - 1;
        std::vector<char> outVector(len);
        int used = len;
        char *out = outVector.data();
        const char *p = name.c_str();
        const char *prefix = p;
        int up = 0;
        const int prefixLength = rootLength(name, allowUncPaths);
        p += prefixLength;
        i -= prefixLength;
        // replicate trailing slash (i > 0 checks for emptiness of input string p)
        // except for remote paths because there can be /../ or /./ ending
        if (i > 0 && p[i] == '/' && !isRemote) {
            out[--used] = '/';
            --i;
        }
        auto isDot = [](const char *p, int i) {
            return i > 1 && p[i - 1] == '.' && p[i - 2] == '/';
        };
        auto isDotDot = [](const char *p, int i) {
            return i > 2 && p[i - 1] == '.' && p[i - 2] == '.' && p[i - 3] == '/';
        };
        while (i >= 0) {
            // copy trailing slashes for remote urls
            if (p[i] == '/') {
                if (isRemote && !up) {
                    if (isDot(p, i)) {
                        i -= 2;
                        continue;
                    }
                    out[--used] = p[i];
                }
                --i;
                continue;
            }
            // remove current directory
            if (p[i] == '.' && (i == 0 || p[i-1] == '/')) {
                --i;
                continue;
            }
            // detect up dir
            if (i >= 1 && p[i] == '.' && p[i-1] == '.' && (i < 2 || p[i - 2] == '/')) {
                ++up;
                i -= i >= 2 ? 3 : 2;
                if (isRemote) {
                    // moving up should consider empty path segments too (/path//../ -> /path/)
                    while (i > 0 && up && p[i] == '/') {
                        --up;
                        --i;
                    }
                }
                continue;
            }
            // prepend a slash before copying when not empty
            if (!up && used != len && out[used] != '/')
                out[--used] = '/';
            // skip or copy
            while (i >= 0) {
                if (p[i] == '/') {
                    // copy all slashes as is for remote urls if they are not part of /./ or /../
                    if (isRemote && !up) {
                        while (i > 0 && p[i] == '/' && !isDotDot(p, i)) {
                            if (isDot(p, i)) {
                                i -= 2;
                                continue;
                            }
                            out[--used] = p[i];
                            --i;
                        }
                        // in case of /./, jump over
                        if (isDot(p, i))
                            i -= 2;
                        break;
                    }
                    --i;
                    break;
                }
                // actual copy
                if (!up)
                    out[--used] = p[i];
                --i;
            }
            // decrement up after copying/skipping
            if (up)
                --up;
        }
        // Indicate failure when ".." are left over for an absolute path.
        if (ok)
            *ok = prefixLength == 0 || up == 0;
        // add remaining '..'
        while (up && !isRemote) {
            if (used != len && out[used] != '/') // is not empty and there isn't already a '/'
                out[--used] = '/';
            out[--used] = '.';
            out[--used] = '.';
            --up;
        }
        bool isEmpty = used == len;
        if (prefixLength) {
            if (!isEmpty && out[used] == '/') {
                // Eventhough there is a prefix the out string is a slash. This happens, if the input
                // string only consists of a prefix followed by one or more slashes. Just skip the slash.
                ++used;
            }
            for (int i = prefixLength - 1; i >= 0; --i)
                out[--used] = prefix[i];
        } else {
            if (isEmpty) {
                // After resolving the input path, the resulting string is empty (e.g. "foo/.."). Return
                // a dot in that case.
                out[--used] = '.';
            } else if (out[used] == '/') {
                // After parsing the input string, out only contains a slash. That happens whenever all
                // parts are resolved and there is a trailing slash ("./" or "foo/../" for example).
                // Prepend a dot to have the correct return value.
                out[--used] = '.';
            }
        }
        // If path was not modified return the original value
        if (used == 0)
            return name;
        return std::string(out + used, len - used);
    }
    
    enum {
#if defined(_WIN32)
        OSSupportsUncPaths = true
#else
        OSSupportsUncPaths = false
#endif
    };
    
    std::string cleanPath(const std::string &path, bool *ok)
    {
        if (path.empty())
            return path;
        std::string name = path;
        
        char dir_separator = separator();
        if (dir_separator != '/') {
            for (std::size_t i = 0; i < name.size(); ++i) {
                if (name[i] == dir_separator) {
                    name[i] = '/';
                }
            }
        }
        std::string ret = qt_normalizePathSegments(name, OSSupportsUncPaths ? AllowUncPaths : DefaultNormalization, ok);
        // Strip away last slash except for root directories
        if (ret.size() > 1 && endsWith(ret, std::string("/"))) {
#if defined (_WIN32)
            if (!(ret.size() == 3 && ret[1] == ':'))
#endif
                ret = ret.substr(0, ret.size() - 1);
        }
        return ret;
    }
    
    std::string
    relativeFilePath(const std::string& dirPath, const std::string &absoluteFilePath) {
        if (isRelative(absoluteFilePath) || isRelative(dirPath))
            return absoluteFilePath;
        std::string dir = cleanPath(dirPath);
        std::string file = cleanPath(absoluteFilePath);
#ifdef _WIN32
        std::string dirDrive = driveSpec(dir);
        std::string fileDrive = driveSpec(file);
        bool fileDriveMissing = false;
        if (fileDrive.empty()) {
            fileDrive = dirDrive;
            fileDriveMissing = true;
        }
        if (tolower(fileDrive) != tolower(dirDrive)
            || (startsWith(file, "//")
                && !startsWith(dir, "//")))
            return file;
        dir.erase(0, dirDrive.size());
        if (!fileDriveMissing)
            file.erase(0, fileDrive.size());
#endif
        std::string result;
        std::vector<std::string> dirElts = split(dir, '/');
        std::vector<std::string> fileElts = split(file, '/');
        int i = 0;
        while (i < dirElts.size() && i < fileElts.size() &&
#if defined(_WIN32)
               compareCaseInsensitive(dirElts[i], fileElts[i]) == 0)
#else
            dirElts[i] == fileElts[i])
#endif
            ++i;
        for (int j = 0; j < dirElts.size() - i; ++j)
            result += std::string("../");
        for (int j = i; j < fileElts.size(); ++j) {
            result += fileElts[j];
            if (j < fileElts.size() - 1)
                result += '/';
        }
        if (result.empty())
            return ".";
        return result;
    }

} // namespace StrUtils
