/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>
#include <iterator>
#include <algorithm>
#include <set>
#include <memory>
#include <bitset>
#include <unordered_map>
#include <clang/Basic/SourceLocation.h>
#include "qbjs.h"
#include "clangversionabstraction.h"

class MocPPCallbacks;
class MocASTConsumer;
namespace clang {
class CXXMethodDecl;
class CXXRecordDecl;
class CXXConstructorDecl;
class NamespaceDecl;
class EnumDecl;
class Preprocessor;
class Sema;
class TypeDecl;
class Type;
class Decl;
class Expr;
class QualType;
}

struct NotifyDef {
    std::string Str;
    clang::SourceLocation Loc;
    clang::CXXMethodDecl *MD = nullptr;
    int notifyId = -1;
};

struct PrivateSlotDef {
    std::string ReturnType;
    std::string Name;
    std::vector<std::string> Args;
    int NumDefault = 0;
    std::string InPrivateClass;
};

struct PropertyDef {
    std::string name, type, member, read, write, reset, designable = "true", scriptable = "true", editable, stored = "true",
                user = "false", inPrivateClass;
    NotifyDef notify;

    bool constant = false;
    bool final = false;

    bool isEnum = false;

    int revision = 0;
    bool PointerHack = false; // If the READ method returns a pointer to the type
    bool PossiblyForwardDeclared = false;  //if the type is only forward declared
    bool readOnly = false;
};

struct LAPropertyDef {
    std::string pascalCaseName, camelCaseName;
    std::string lowercaseName;
    std::string defaultValue;
    std::string paramType;
    bool readOnly = false;
    bool isList = false;
    unsigned int protocolVersion;
};

struct TimeFunctionParamDef
{
    std::string lowerCaseName, valueType;
    std::vector<std::string> defaultValues;
    bool isImage = false;
    bool isMask = false;
    bool isMandatory = false;
    int arrayDimension = 1;
    std::shared_ptr<std::bitset<4>> supportedChans;
    std::shared_ptr<std::bitset<4>> supportedDepths;
};

struct PluginData {
    std::string IID;
    QBJS::Value MetaData;
};

struct BaseDef {
    std::vector<std::tuple<clang::EnumDecl*, std::string, bool>> QtEnums,BlxEnums;

    void addEnum(clang::EnumDecl *E, std::string Alias, bool IsFlag, bool Qt) {
        std::vector<std::tuple<clang::EnumDecl*, std::string, bool>>& enums = Qt ? QtEnums : BlxEnums;
        for (auto I : enums)
            if (std::get<1>(I) == Alias)
                return;

        enums.emplace_back(E, std::move(Alias), IsFlag);
     
    }

    std::vector<clang::CXXRecordDecl *> QtExtra,BlxExtra;
    void addExtra(clang::CXXRecordDecl *E,bool Qt) {
        if (!E)
            return;
        std::vector<clang::CXXRecordDecl *>& extra = Qt ? QtExtra : BlxExtra;
        if (std::find(extra.begin(), extra.end(), E) != extra.end())
            return;
        extra.push_back(E);
    }

    std::vector<std::pair<std::string, std::string>> QtClassInfo, BlxClassInfo;
};

struct ClassDef : BaseDef {

    
    void checkProperties(std::vector<PropertyDef>& props, bool qt, clang::Sema& Sema);
    
    clang::CXXRecordDecl *Record = nullptr;

    // This list only includes the things registered with the keywords
    std::vector<clang::CXXMethodDecl*> QtSignals, BlxSignals;
    std::vector<clang::CXXMethodDecl*> Slots;
    std::vector<PrivateSlotDef> PrivateSlots;
    std::vector<clang::CXXMethodDecl*> QtMethods, BlxMethods;
    std::vector<clang::CXXConstructorDecl*> QtConstructors, BlxConstructors;


    std::vector<std::string> Interfaces;
    PluginData Plugin;

    std::vector<PropertyDef> QtProperties, BlxProperties;
    std::vector<LAPropertyDef> LAProperties;
    std::vector<TimeFunctionParamDef> TimeFunctionParams;
    std::string LAAttachedPropertiesType;
    bool has_la_properties = false;
    bool HasQObject = false;
    bool HasQGadget = false;
    bool HasBlxClass = false;
    bool HasBlxValueType = false;
    bool hasTimeFunctionDefine = false;
    
    int QtNotifyCount = 0;
    int BlxNotifyCount = 0;
    int PrivateSlotCount = 0;
    int QtRevisionPropertyCount = 0;
    int BlxRevisionPropertyCount = 0;
    int QtRevisionMethodCount = 0;
    int BlxRevisionMethodCount = 0;
};

struct NamespaceDef : BaseDef {
    clang::NamespaceDecl *Namespace = nullptr;
    bool hasQNamespace = false;
    bool hasBlxNamespace = false;
};

class MocNg {
public:

    typedef std::set<const clang::Type*> MetaTypeSet;
    MetaTypeSet registered_meta_type;

    typedef std::unordered_map<std::string, const clang::CXXRecordDecl*> InterfaceMap;
    InterfaceMap interfaces;

    ClassDef parseClass (MocASTConsumer* ast, clang::CXXRecordDecl* RD, clang::Sema& Sema);
    NamespaceDef parseNamespace(clang::NamespaceDecl* ND, clang::Sema& Sema);
    static bool IsAnnotationStaticAssert(clang::Decl *Decl, llvm::StringRef *Key, clang::Expr **SubExp);

    bool HasPlugin = false;
    bool hasLAProperties = false;
    bool hasTimeFunctionDefine = false;

    std::map<clang::SourceLocation, std::string> Tags;
    std::string GetTag(clang::SourceLocation DeclLoc, const clang::SourceManager& SM);
    bool ShouldRegisterMetaType(clang::QualType T);
};
