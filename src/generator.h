/****************************************************************************
 *  Copyright (C) 2013-2016 Woboq GmbH
 *  Olivier Goffart <contact at woboq.com>
 *  https://woboq.com/
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <vector>

namespace clang {
class ASTContext;
class CXXMethodDecl;
class SourceManager;
class QualType;
}

#include <clang/AST/PrettyPrinter.h>
#include "mocng.h"

struct ClassDef;



// From qmetaobject_p.h
enum class QtPropertyFlags  {
    Invalid = 0x00000000,
    Readable = 0x00000001,
    Writable = 0x00000002,
    Resettable = 0x00000004,
    EnumOrFlag = 0x00000008,
    StdCppSet = 0x00000100,
//     Override = 0x00000200,
    Constant = 0x00000400,
    Final = 0x00000800,
    Designable = 0x00001000,
    ResolveDesignable = 0x00002000,
    Scriptable = 0x00004000,
    ResolveScriptable = 0x00008000,
    Stored = 0x00010000,
    ResolveStored = 0x00020000,
    Editable = 0x00040000,
    ResolveEditable = 0x00080000,
    User = 0x00100000,
    ResolveUser = 0x00200000,
    Notify = 0x00400000,
    Revisioned = 0x00800000
};
enum class QtMethodFlags  {
    AccessPrivate = 0x00,
    AccessProtected = 0x01,
    AccessPublic = 0x02,
    AccessMask = 0x03, //mask
    MethodMethod = 0x00,
    MethodSignal = 0x04,
    MethodSlot = 0x08,
    MethodConstructor = 0x0c,
    MethodTypeMask = 0x0c,
    MethodCompatibility = 0x10,
    MethodCloned = 0x20,
    MethodScriptable = 0x40,
    MethodRevisioned = 0x80
};
enum class QtMetaObjectFlags {
    DynamicMetaObject = 0x01,
    RequiresVariantMetaObject = 0x02,
    PropertyAccessInStaticMetaCall = 0x04
};
enum class QtMetaDataFlags : std::uint64_t {
    IsUnresolvedType = 0x80000000,
    TypeNameIndexMask = 0x7FFFFFFF
};


enum class BlxMetaPropertyFlag  {
    Invalid = 0,
    Readable = 1 << 0,
    Writable = 1 << 1,
    Resettable = 1 << 2,
    EnumOrFlag = 1 << 3,
    Constant = 1 << 4,
    Notify = 1 << 5,
    Revisioned = 1 << 6
};

enum class BlxMetaMethodFlag  {
    AccessPrivate = 0,
    AccessProtected = 1 << 0,
    AccessPublic = 1 << 1,
    AccessMask = 0x03, //mask

    MethodMethod = 0,
    MethodSignal = 1 << 2,
    MethodSlot = 1 << 3,
    MethodConstructor = 1 << 4,
    MethodTypeMask = 1 << 4,

    MethodCloned = 1 << 5,
    MethodRevisioned = 1 << 6
};

enum BlxMetaObjectFlag {
    DynamicMetaObject = 0x01,
};


enum class BlxMetaDataFlag : std::uint64_t {
    IsUnresolvedType = 0x80000000,
    TypeNameIndexMask = 0x7FFFFFFF,
    IsUnresolvedSignal = 0x70000000
};

enum { QtOutputRevision = 8,
        BlxOutputRevision = 1,
       MetaObjectPrivateFieldCount = 14, //  = sizeof(QMetaObjectPrivate) / sizeof(int)
       mocOutputRevision = 67,
       QT_VERSION = 0x050100
};


enum CodeGenType
{
    eCodeGenQt,
    eCodeGenBlx
};


template <CodeGenType type>
struct EnumsHelper
{
    
};

template <>
struct EnumsHelper<eCodeGenQt>
{
    using MetaDataFlags = QtMetaDataFlags;
    using MetaObjectFlags = QtMetaObjectFlags;
    using MetaMethodFlags = QtMethodFlags;
    using MetaPropertyFlags = QtPropertyFlags;
};

template <>
struct EnumsHelper<eCodeGenBlx>
{
    using MetaDataFlags = BlxMetaDataFlag;
    using MetaObjectFlags = BlxMetaObjectFlag;
    using MetaMethodFlags = BlxMetaMethodFlag;
    using MetaPropertyFlags = BlxMetaPropertyFlag;
};

#define MOCNG_VERSION_STR "1.0"

class Generator {
    const BaseDef *Def;
    const ClassDef *CDef;
    const NamespaceDef *NDef;
    llvm::raw_ostream& OS;
    llvm::raw_ostream& OS_TemplateHeader;

    std::vector<std::string> Strings;

    std::string Name,QualName;
    std::string QualifiedClassNameIdentifier;
    std::string propertyContainerBase;
    std::string qBaseName;
    std::string timeFunctionsBaseName;
    std::string blxBaseName;
    std::string TemplatePrefix; // what is in front of the template declaration ("template<typename t>")
    bool BaseHasStaticMetaObject = false;
    bool BaseHasStaticBlxMetaObject = false;
    bool HasTemplateHeader;
    int QtMethodCount = 0, BlxMethodCount = 0;

    clang::ASTContext &Ctx;
    clang::PrintingPolicy PrintPolicy;

    MocNg *Moc;

public:
    explicit Generator(const ClassDef *CDef, llvm::raw_ostream& OS, clang::ASTContext & Ctx, MocNg *Moc,
              llvm::raw_ostream *OS_TemplateHeader = nullptr);
    // For namespaces
    explicit Generator(const NamespaceDef *NDef, llvm::raw_ostream& OS, clang::ASTContext & Ctx, MocNg *Moc);

    bool IsQtNamespace = false;

    // plugin metadata from -M command line argument  (to be put in the JSON)
    std::vector<std::pair<llvm::StringRef, llvm::StringRef>> MetaData;

    void GenerateCode();
private:
   
    void GenerateCodeInternal(CodeGenType genType);

    int StrIdx(llvm::StringRef);
    template <typename T>
    void GenerateFunctions(const std::vector<T> &V, const char *TypeName, QtMethodFlags Type, int &ParamIndex);
    template <typename T>
    void GenerateBlxFunctions(const std::vector<T> &V, const char *TypeName, BlxMetaMethodFlag Type, int &ParamIndex);
    template <typename T>
    void GenerateFunctionParameters(const std::vector<T*> &V, const char *TypeName, CodeGenType type);

    void GenerateProperties();
    void GenerateBlxProperties();
    void GenerateMetaCall();
    void GenerateBlxMetaCall();
    void GenerateStaticMetaCall();
    void GenerateBlxStaticMetaCall();
    void GenerateSignal(const clang::CXXMethodDecl *MD, int Idx);
    void GenerateLAProperties();
    void GenerateTimeFunctionData();

    void GenerateTypeInfo(clang::QualType Type, CodeGenType type);
    void GenerateEnums(int EnumIndex, CodeGenType type);
    void GeneratePluginMetaData(bool Debug);

    // Called when emiting the code to generate the invokation of a method.
    // Return true if the code was already emitted  (include the break;)
    // defined in workaroundtest.cpp
    static bool WorkaroundTests(llvm::StringRef ClassName, const clang::CXXMethodDecl* MD, llvm::raw_ostream &OS);
};
